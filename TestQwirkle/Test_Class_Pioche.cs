﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;

namespace TestQwirkle
{
    [TestClass]
    public class Test_Class_Pioche
    {
        [TestMethod]
        public void Test_NombreTuilePioche()
        {
            Pioche Pioche_de_Test = new Pioche();
            Tuile Tuile_Test = new Tuile(1, 1);

            Assert.AreEqual(108, Pioche_de_Test.Get_Count_Liste());

            Pioche_de_Test.Remise_Tuile(6);
            Assert.AreEqual(102, Pioche_de_Test.Get_Count_Liste());

            Pioche_de_Test.Remise_Tuile_Swap(Tuile_Test);
            Assert.AreEqual(102, Pioche_de_Test.Get_Count_Liste());
        }
    }
}
