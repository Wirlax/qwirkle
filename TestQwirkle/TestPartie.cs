﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;
using System.Collections.Generic;

namespace TestQwirkle
{
    [TestClass]
    public class TestPartie
    {
        [TestMethod]
        public void Testquiquicommence()
        {
            DateTime datenais1 = new DateTime(2000, 5, 3);
            DateTime datenais2 = new DateTime(2000, 5, 2);
            Joueur joueur1 = new Joueur("Ronan", datenais1, 1);
            Joueur joueur2 = new Joueur("Julien", datenais2, 2);
            Partie partietest = new Partie(30, 30, joueur1, joueur2);
            Assert.AreEqual(2, partietest.quiquicomence());
        }
        [TestMethod]
        public void Testveriffinpartie()
        {
            DateTime datenais1 = new DateTime(2000, 5, 3);
            DateTime datenais2 = new DateTime(2000, 5, 2);
            Joueur joueur1 = new Joueur("Ronan", datenais1, 1);
            Joueur joueur2 = new Joueur("Julien", datenais2, 2);
            Partie partietest = new Partie(30, 30, joueur1, joueur2);
            for (int i = 0; i < 6; i++)
            {
                joueur1.GetMain().Add(new Tuile());
                joueur2.GetMain().Add(new Tuile());
            }
            Assert.AreEqual(true, partietest.Veriffinpartie());
            joueur2.GetMain().RemoveAt(5);
            joueur2.GetMain().Add(new Tuile(1,1));
            Assert.AreEqual(false, partietest.Veriffinpartie());
            Assert.AreEqual(true, joueur1.Getafini());
        }
    }
}
