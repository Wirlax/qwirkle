﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass; 

namespace TestQwirkle
{
    [TestClass]
    public class Test_Class_Tuile
    {
        [TestMethod]
        public void Test_AcesseurGet_Couleur()
        {
            Tuile Test = new Tuile(1,3);
            Assert.AreEqual(1, Test.GetCouleur());
            Assert.AreEqual(3, Test.GetForme());
        }

        [TestMethod]
        public void Test_AcesseurSet_Couleur()
        {
            Tuile Test = new Tuile(1, 3);
            Test.SetCouleur(4); 
            Assert.AreEqual(4, Test.GetCouleur());
            Test.SetForme(1);
            Assert.AreEqual(1, Test.GetForme());
        }
    }
}
