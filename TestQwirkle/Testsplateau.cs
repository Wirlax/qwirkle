﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;

namespace TestQwirkle
{
    [TestClass]
    public class TestPlateau
    {
        [TestMethod]
        public void Testplacetuile()
        {
            Plateau testplateau = new Plateau(5, 5);
            Tuile tuile1 = new Tuile(1, 1);
            testplateau.Placetuile(3, 2,0,0,tuile1);
            Assert.AreEqual(1, (testplateau.getTuile(3,2)).GetCouleur());
            Plateau testplateau4 = new Plateau(2, 5);
            Tuile tuile4 = new Tuile();
            testplateau4.Placetuile(2, 1,0,0, tuile4);
            Assert.AreEqual(-1, (testplateau4.getTuile(1, 1)).GetCouleur());
            Assert.AreEqual(testplateau4.Placetuile(2, 1,0,0, tuile1),false);
        }
/*        [TestMethod]
        public void Testverifcouleur()
        {
            Plateau plateau = new Plateau(10, 10);
            Tuile tuile1 = new Tuile(1, 1);
            Tuile tuile2 = new Tuile(2, 1);
            Tuile tuile3 = new Tuile(3, 1);
            Tuile tuile4 = new Tuile(4, 1);
            plateau.Placetuile(2, 1,0,0, tuile1);
            plateau.Placetuile(3, 1,0,0, tuile2);
            plateau.Placetuile(4, 1,0,0, tuile3);
            Assert.AreEqual(plateau.Verifcouleur(5, 1, 0,1),true);
            Assert.AreEqual(plateau.Verifcouleur(5, 1, 2,1), false);
            Assert.AreEqual(plateau.Verifcouleur(1, 1, 0,1), true);
            Assert.AreEqual(plateau.Verifcouleur(3, 2, 0,1), true);
            Assert.AreEqual(plateau.Verifcouleur(3, 2, 2,1), false);
            Assert.AreEqual(plateau.Verifcouleur(5, 1, 5,1), true);
            Assert.AreEqual(plateau.Verifcouleur(2, 0, 1,1), false);
        }
        [TestMethod]
        public void Testverifforme()
        {
            Plateau plateau = new Plateau(10, 10);
            Tuile tuile1 = new Tuile(1, 1);
            Tuile tuile2 = new Tuile(1, 2);
            Tuile tuile3 = new Tuile(1, 3);
            Tuile tuile4 = new Tuile(1, 4);
            plateau.Placetuile(2, 1,0,0, tuile1);
            plateau.Placetuile(3, 1,0,0, tuile2);
            plateau.Placetuile(4, 1,0,0, tuile3);
            Assert.AreEqual(plateau.Verifforme(5, 1, 0,1), true);
            Assert.AreEqual(plateau.Verifforme(5, 1, 2,1), false);
            Assert.AreEqual(plateau.Verifforme(1, 1, 0,1), true);
            Assert.AreEqual(plateau.Verifforme(3, 2, 0,1), true);
            Assert.AreEqual(plateau.Verifforme(3, 2, 2,1), false);
            Assert.AreEqual(plateau.Verifforme(5, 1, 5,1), true);
            Assert.AreEqual(plateau.Verifforme(2, 0, 1,1), false);
        }*/
        [TestMethod]
        public void Testcheckcasesautour()
        {
            Plateau plateau = new Plateau(6, 6);
            Tuile tuile1 = new Tuile(5, 2);
            Tuile tuile2 = new Tuile(1, 1);
            plateau.Placetuile(2, 4,0,0,tuile1);
            int[] tab;
            tab = plateau.Checkcaseautour(2, 4);
            for (int i = 0; i < 4; i++)
            {
            Assert.AreEqual(tab[i], 0);
            }
            plateau.Placetuile(2, 5,0,0, tuile2);
            tab = plateau.Checkcaseautour(2, 4);
            Assert.AreEqual(tab[2], 1);
        }
        [TestMethod]
        public void Testcalculscore()
        {
            Plateau plateau = new Plateau(10, 10);
            Tuile tuile1 = new Tuile(1, 1);
            Tuile tuile2 = new Tuile(1, 2);
            Tuile tuile3 = new Tuile(1, 3);
            Tuile tuile4 = new Tuile(1, 4);
            plateau.Placetuile(2, 1,0,0, tuile1);
            plateau.Placetuile(3, 1,0,0, tuile2);
            plateau.Placetuile(4, 1,0,0, tuile3);
            Assert.AreEqual(3, plateau.Calculscore(5, 1));
        }
        /*[TestMethod]
        public void Testveriftout()
        {
            Plateau plateau = new Plateau(10, 10);
            Tuile tuile1 = new Tuile(1, 1);
            Tuile tuile2 = new Tuile(1, 2);
            Tuile tuile3 = new Tuile(1, 3);
            Tuile tuile4 = new Tuile(1, 4);
            plateau.Placetuile(2, 1,0,0, tuile1);
            plateau.Placetuile(3, 1,0,0, tuile2);
            plateau.Placetuile(4, 1,0,0, tuile3);
            Assert.AreEqual(1, plateau.Veriftout(5, 1,0,0, 0, 0)[0]);
            Assert.AreEqual(3, plateau.Veriftout(5, 1,0,0, 0, 0)[1]);
            Assert.AreEqual(0, plateau.Veriftout(5, 1,0,0, 0, 2)[0]);
            Assert.AreEqual(0, plateau.Veriftout(5, 1,0,0, 0, 2)[1]);

        }*/
    }
}
