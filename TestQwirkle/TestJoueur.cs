﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleClass;

namespace TestQwirkle
{
    [TestClass]
    public class TestJoueur
    {

        [TestMethod]
        public void Test_Get_Pseudo()
        {
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Assert.AreEqual("Jean", Test.GetPseudo());
        }
        [TestMethod]
        public void Test_Set_Pseudo()
        {
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Test.SetPseudo("Louis");
            Assert.AreEqual("Louis", Test.GetPseudo());
        }
        [TestMethod]
        public void Test_Get_Score()
        {
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Assert.AreEqual(0, Test.GetScore());
        }
        [TestMethod]
        public void Test_Set_Score()
        {
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Test.SetScore(50);
            Assert.AreEqual(50, Test.GetScore());
        }
        [TestMethod]
        public void Test_Get_NbPieceMain()
        {
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Assert.AreEqual(0, Test.GetNbPieceMain());
            Test.GetMain().Add(new Tuile(1, 1));
            Assert.AreEqual(1, Test.GetNbPieceMain());
        }

        [TestMethod]
        public void TestAddScore()
        {
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Test.AddScore(5);
            Assert.AreEqual(5, Test.GetScore());
        }
        [TestMethod]
        public void TestVerifNbPieceManquante()
        {
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Test.GetMain().Add(new Tuile(1, 1));
            Assert.AreEqual(5, Test.VerifNbPieceManquante());
        }
        [TestMethod]
        public void TestSetMain()
        {
            Pioche PiocheTest = new Pioche();
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Test.SetMain(PiocheTest);
            Assert.AreEqual(6, Test.GetMain().Count);
        }
        [TestMethod]
        public void TestRemiseFinDeTour()
        {
            Pioche PiocheTest = new Pioche();
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Test.Remise_fin_de_tour(2,PiocheTest);
            Assert.AreEqual(2, Test.GetMain().Count);
        }
        [TestMethod]
        public void TestSwapTuile()
        {
            Pioche PiocheTest = new Pioche();
            DateTime DateTest = new DateTime(1998, 02, 01);
            Joueur Test = new Joueur("Jean", DateTest,1);
            Test.GetMain().Add(new Tuile(1, 2)); Test.GetMain().Add(new Tuile(1, 2));
            Test.Swap_Tuile(1,PiocheTest);
            Assert.AreEqual(2, Test.GetMain().Count);
        }
    }
}
