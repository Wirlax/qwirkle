﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Tuile
    {
        private int aT_Couleur;
        private int aT_Forme;
        //private int Numéro; 
        public Tuile(int p_Couleur, int p_Forme) { this.aT_Couleur = p_Couleur; this.aT_Forme = p_Forme; }  //Constructeur de base de la Tuile, avec en paramètre une Couleur et une Forme 
        public Tuile() { this.aT_Couleur = -1;this.aT_Forme = -1; }
        //Ancien constructeur qui créer une Tuile '-1,-1' qui n'avait donc pas de forme et de couleur (car a la base nous fonctionnions avec un tableau, qui devait donc être combler quand une Tuile était retirer
        public int GetCouleur() { return aT_Couleur; } //Accesseur en lecture retournant la couleur de la Tuile 
        public int GetForme() { return aT_Forme; }   //Accesseur en lecture retournant la forme de la Tuile
        public void SetCouleur(int p_NewCouleur) { this.aT_Couleur = p_NewCouleur; } //Accesseur en ecriture modifiant la couleur (n'est pas utile suaf pour les tests) 
        public void SetForme(int p_NewForme) { this.aT_Forme = p_NewForme; } //Accesseur en ecriture modifiant la forme (n'est pas utile suaf pour les tests)  




    }
}