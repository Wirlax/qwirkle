﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Plateau
    {
        Case[,] tab_plat;
        bool premiermot;
        int largeur;
        int longueur;
        public Plateau(int largeur, int longueur)
        {
            this.tab_plat = new Case[largeur, longueur];
            this.Largeur = largeur;
            this.Longueur = longueur;
            for (int index_x = 0; index_x < largeur; index_x++)
            {
                for (int index_y = 0; index_y < longueur; index_y++)
                {
                    this.tab_plat[index_x, index_y] = new Case(index_x, index_y);
                    this.tab_plat[index_x, index_y].settuile(new Tuile());
                }
            }
            premiermot = true;
        }
        public Case[,] Tab_plat { get => tab_plat; set => tab_plat = value; }
        public int Largeur { get => largeur; set => largeur = value; }
        public int Longueur { get => longueur; set => longueur = value; }
        public Tuile getTuile(int posx, int posy)
        {
            return tab_plat[posx, posy].getQuelletuile;
        }
        public int[] Checkcaseautour(int posx, int posy)
        {
            int compteur = 0;
            int[] tab = { -1, -1, -1, -1 ,-1};
            if (this.getTuile(posx, posy - 1).GetCouleur() == -1 && this.getTuile(posx, posy - 1).GetForme() == -1)
            {
                tab[0] = 0;
            }
            else
            {
                tab[0] = 1;
                compteur++;
            }
            if (this.getTuile(posx + 1, posy).GetCouleur() == -1 && this.getTuile(posx + 1, posy).GetForme() == -1)
            {
                tab[1] = 0;
            }
            else
            {
                tab[1] = 1;
                compteur++;
            }
            if (this.getTuile(posx, posy + 1).GetCouleur() == -1 && this.getTuile(posx, posy + 1).GetForme() == -1)
            {
                tab[2] = 0;
            }
            else
            {
                tab[2] = 1;
                compteur++;
            }
            if (this.getTuile(posx - 1, posy).GetCouleur() == -1 && this.getTuile(posx - 1, posy).GetForme() == -1)
            {
                tab[3] = 0;
            }
            else
            {
                tab[3] = 1;
                compteur++;
            }
            tab[4] = compteur;
            return tab;
        }
        public int Calculscore(int posx, int posy)
        {
            int score = 1;
            int[] tab = Checkcaseautour(posx, posy);
            int compteurqwirklescoreplussixomgnewstratopshit = 0;
            for (int index_direction = 0; index_direction < 4; index_direction++)
            {
                compteurqwirklescoreplussixomgnewstratopshit = 0;
                int differencex = 0;
                int differencey = 0;
                if (tab[index_direction] == 1)
                {
                    if (index_direction == 0) differencey = -1;
                    if (index_direction == 2) differencey = 1;
                    if (index_direction == 1) differencex = 1;
                    if (index_direction == 3) differencex = -1;
                    while (this.getTuile(posx + differencex, posy + differencey).GetForme() != -1 && compteurqwirklescoreplussixomgnewstratopshit < 7)
                    {
                        score++;
                        compteurqwirklescoreplussixomgnewstratopshit++;
                        if (differencex < 0) differencex--;
                        if (differencex > 0) differencex++;
                        if (differencey < 0) differencey--;
                        if (differencey > 0) differencey++;
                    }
                }
            }
            if (compteurqwirklescoreplussixomgnewstratopshit == 6) { score += 6; }
            return score;
        }
        public int[] Veriftout(int posx, int posy, int differencex, int differencey, int couleuraverif, int formeaverif)
        {
            int bonneposx = posx + differencex;
            int bonneposy = posy + differencey;
            int[] tableauretour = { 0, 0 };
            if (premiermot == true)
            {
                tableauretour[0] = 1;
            }
            else
            {
                if (this.getTuile(bonneposx, bonneposy).GetCouleur() == -1 && this.getTuile(bonneposx, bonneposy).GetForme() == -1)
                {
                    if ((this.verifmemecouleurautreforme(bonneposx, bonneposy, formeaverif, couleuraverif) ^ this.verifmemeformeautrecouleur(bonneposx, bonneposy, couleuraverif, formeaverif)) || Checkcasrelou(bonneposx,bonneposy,couleuraverif,formeaverif))
                    {
                        tableauretour[0] = 1;
                        tableauretour[1] = this.Calculscore(bonneposx, bonneposy);
                        return tableauretour;
                    }
                }
            }
            return tableauretour;
        }
        public bool Placetuile(int posx, int posy, int differencex, int differencey, Tuile tuieaplacer)
        {
            if (premiermot == true) { premiermot = false; }
            int bonneposx = posx + differencex;
            int bonneposy = posy + differencey;
            try
            {
                if (this.tab_plat[bonneposx, bonneposy].getQuelletuile.GetCouleur() == -1)
                {
                    this.tab_plat[bonneposx, bonneposy].settuile(tuieaplacer);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public bool verifmemeformeautrecouleur(int posx, int posy, int couleuraverif, int formeaverif)
        {
            try
            {
                int[] tab = Checkcaseautour(posx, posy);
                bool flag = true;
                bool formelameme = true;
                for (int index_direction = 0; index_direction < 4; index_direction++)
                {
                    int differencex = 0;
                    int differencey = 0;
                    if (tab[index_direction] == 1)
                    {
                        int[] tableaudescouleursvalides = { 1, 1, 1, 1, 1, 1 };
                        if (index_direction == 0) differencey = -1;
                        if (index_direction == 2) differencey = 1;
                        if (index_direction == 1) differencex = 1;
                        if (index_direction == 3) differencex = -1;

                        while (this.getTuile(posx + differencex, posy + differencey).GetCouleur() != -1)
                        {
                            tableaudescouleursvalides[this.getTuile(posx + differencex, posy + differencey).GetCouleur()] = 0;
                            if (this.getTuile(posx + differencex, posy + differencey).GetForme() != formeaverif) formelameme = false;
                            if (differencex < 0) differencex--;
                            if (differencex > 0) differencex++;
                            if (differencey < 0) differencey--;
                            if (differencey > 0) differencey++;
                        }
                        if (index_direction == 0) differencey = -1;
                        if (index_direction == 2) differencey = 1;
                        if (index_direction == 1) differencex = 1;
                        if (index_direction == 3) differencex = -1;
                        if (tableaudescouleursvalides[couleuraverif] == 1 && formelameme == true)
                        {
                            flag = true;
                        }
                        else { flag = false; }
                    }
                }
                if (flag == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        public bool verifmemecouleurautreforme(int posx, int posy, int formeaverif, int couleuraverif)
        {
            {
                try
                {
                    int[] tab = Checkcaseautour(posx, posy);
                    bool flag = true;
                    bool couleurlameme = true;
                    for (int index_direction = 0; index_direction < 4; index_direction++)
                    {
                        int differencex = 0;
                        int differencey = 0;
                        if (tab[index_direction] == 1)
                        {
                            int[] tableaudesformesvalides = { 1, 1, 1, 1, 1, 1 };
                            if (index_direction == 0) differencey = -1;
                            if (index_direction == 2) differencey = 1;
                            if (index_direction == 1) differencex = 1;
                            if (index_direction == 3) differencex = -1;

                            while (this.getTuile(posx + differencex, posy + differencey).GetForme() != -1)
                            {
                                tableaudesformesvalides[this.getTuile(posx + differencex, posy + differencey).GetForme()] = 0;
                                if (this.getTuile(posx + differencex, posy + differencey).GetCouleur() != couleuraverif) couleurlameme = false;
                                if (differencex < 0) differencex--;
                                if (differencex > 0) differencex++;
                                if (differencey < 0) differencey--;
                                if (differencey > 0) differencey++;
                            }
                            if (index_direction == 0) differencey = -1;
                            if (index_direction == 2) differencey = 1;
                            if (index_direction == 1) differencex = 1;
                            if (index_direction == 3) differencex = -1;

                            if (tableaudesformesvalides[formeaverif] == 1 && couleurlameme == true)
                            {
                                flag = true;
                            }
                            else { flag = false; }
                        }
                    }
                    if (flag == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }
        public bool Checkcasrelou(int posx, int posy, int couleuraverif, int formeaverif)
        {
            int differencex1 = 0;
            int differencey1 = 0;
            int differencex2 = 0;
            int differencey2 = 0;
            int[] tab = Checkcaseautour(posx, posy);
            if (tab[4] == 2)
            {
                bool first = true;
                for (int index_direction = 0; index_direction < 4; index_direction++)
                {
                    if (tab[index_direction] == 1 && first == false)
                    {
                        if (index_direction == 0) differencey2 = -1;
                        if (index_direction == 2) differencey2 = 1;
                        if (index_direction == 1) differencex2 = 1;
                        if (index_direction == 3) differencex2 = -1;
                    }
                    if (tab[index_direction] == 1 && first == true)
                    {
                        if (index_direction == 0) differencey1 = -1;
                        if (index_direction == 2) differencey1 = 1;
                        if (index_direction == 1) differencex1 = 1;
                        if (index_direction == 3) differencex1 = -1;
                        first = false;
                    }
                }
            }
            int couleurpiece1 = this.getTuile(posx + differencex1, posy + differencey1).GetCouleur();
            int couleurpiece2 = this.getTuile(posx + differencex2, posy + differencey2).GetCouleur();
            int formepiece1 = this.getTuile(posx + differencex1, posy + differencey1).GetForme();
            int formepiece2 = this.getTuile(posx + differencex2, posy + differencey2).GetForme();
            if ((couleuraverif == couleurpiece1 && couleuraverif != couleurpiece2) && (formeaverif != formepiece1 && formepiece2 == formeaverif) || ((couleuraverif != couleurpiece1 && couleuraverif == couleurpiece2) && (formeaverif == formepiece1 && formepiece2 != formeaverif)))
            {
                return true;
            }
            return false;
        }
    }
}
