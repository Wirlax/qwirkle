﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Joueur
    {
        private string pseudo;
        private DateTime date_nais;
        private int score;
        private List<Tuile> main = new List<Tuile>();
        private int num_joueur;
        private bool afini;

        public Joueur(string pseudo, DateTime date_nais,int num_joueur)
        {
            this.pseudo = pseudo;
            this.date_nais = date_nais;
            this.num_joueur = num_joueur;
            this.afini = false;
        }

        public bool Getafini() { return this.afini; }
        public void setafini(bool valeuraset) { this.afini = valeuraset; }
        public int GetNbPieceMain() { return main.Count; }
        public int GetScore() { return score; }
        public void SetScore(int value_score) { this.score = value_score; }
        public string GetPseudo() { return pseudo; }
        public void SetPseudo(string value_pseudo) { this.pseudo = value_pseudo; }
        public DateTime GetDate() { return date_nais; }
        public void SetDate(DateTime value_date_nais) { this.date_nais = value_date_nais; }
        public int Getnum_joueur() { return num_joueur; }

        //Fonction qui permet d'incrémenter le score prend le nombre de point a ajouter en paramètre
        public void AddScore(int score_a_ajouter)
        {
            try
            {
                this.score = score + score_a_ajouter;
            }

            catch (Exception)
            {
                throw;
            }
        }

        //Fonction qui vérifie le nombre de pièce manquante dans ta main 
        public int VerifNbPieceManquante()
        {
            try
            {
                int NbPieceManquante = 6 - GetNbPieceMain();
                return NbPieceManquante;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<Tuile> GetMain()
        {
            return main;
        }

        //Fonction qui initialise la main du joueur prend la pioche en paramètre 
        public void SetMain(Pioche Pioche_partie)
        {

            try
            {
                List<Tuile> ListeAPrendre= new List<Tuile>();
                ListeAPrendre = Pioche_partie.Remise_Tuile(6);
                for (int index_setmain = 0; index_setmain <= 5; index_setmain++)
                {
                    main.Add(ListeAPrendre.ElementAt(index_setmain));
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        //Fonction qui remplis ta main a la fin du tour prend le nombre de pièce manquante et la pioche en paramètre
        public void Remise_fin_de_tour(int nb_piece, Pioche Pioche_partie)
        {
            try
            {
                List<Tuile> ListeRemise = new List<Tuile>();
                ListeRemise = Pioche_partie.Remise_Tuile(nb_piece);
                for (int index_remise = 0; index_remise <= nb_piece - 1; index_remise++)
                {
                    main.Add(ListeRemise.ElementAt(index_remise));
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        //Fonction qui permet d'envoyer une tuile dans la pioche pour la swap prend en paramètre le numéro de la tuile et la pioche
        public Tuile Swap_Tuile(int num_tuile_a_swap, Pioche Pioche_partie)
        {
            Tuile Tuile_a_Swap = new Tuile();
            Tuile_a_Swap = main.ElementAt(num_tuile_a_swap);
            main.RemoveAt(num_tuile_a_swap);
            main.Add(Pioche_partie.Remise_Tuile_Swap(Tuile_a_Swap));
            return Tuile_a_Swap;
        }
    }
}
