﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Pioche
    {
        private List<Tuile> aP_ListeContenantLaPioche = new List<Tuile>();  //La pioche est représentée par une grande liste de 108 Tuile 
        //private Tuile[] aT_TableauContenantLaPioche = new Tuile[108] ;

        public Pioche()
        {
            int Compteur_Tableau = 0;

            for (int Compteur_Index_Tableau = 0; Compteur_Index_Tableau <= 2; Compteur_Index_Tableau++)
                for (int Compteur_Index_Couleur = 0; Compteur_Index_Couleur <= 5; Compteur_Index_Couleur++)
                    for (int Compteur_Index_Forme = 0; Compteur_Index_Forme <= 5; Compteur_Index_Forme++)
                    {
                        aP_ListeContenantLaPioche.Add(new Tuile(Compteur_Index_Couleur, Compteur_Index_Forme));
                        Compteur_Tableau++;
                    }
        } //Constructeur de la Pioche (initialise une liste de 108 Tuile, correspondant aux couleurs et formes présentes dans le jeu) 

        public Tuile Get_LigneListePioche(int p_num_pieceVoulue)
        {
            return aP_ListeContenantLaPioche.ElementAt(p_num_pieceVoulue);
        } //Accesseur en lecture retournant la liste de la pioche dans sa totalité 

        public int Get_Count_Liste() //Accesseur en lecture retournant le nombre de tuiles présente dans la liste a un instant T 
        {
            return aP_ListeContenantLaPioche.Count;
        }
        public List<Tuile> Remise_Tuile(int p_nbTuileaDonner)
        {
            List<Tuile> ListeRendu = new List<Tuile>();
            Random rnd = new Random();
            for (int indexRemiseTuile = 1; indexRemiseTuile <= p_nbTuileaDonner; indexRemiseTuile++)
            {
                int numeroDeLaTuileRandom = rnd.Next(aP_ListeContenantLaPioche.Count);
                ListeRendu.Add(aP_ListeContenantLaPioche.ElementAt(numeroDeLaTuileRandom));
                aP_ListeContenantLaPioche.RemoveAt(numeroDeLaTuileRandom);
            }
            return ListeRendu;
        } //Méthode permettant la distribution d'un nombre de Tuile, entré en paramètre. Les Tuiles rendus par cette méthode sont supprimer de la Pioche 

        public Tuile Remise_Tuile_Swap(Tuile p_TuileSwaper)
        {
            aP_ListeContenantLaPioche.Add(p_TuileSwaper);
            Random rnd = new Random();
            int numeroDeLaTuileRandom = rnd.Next(aP_ListeContenantLaPioche.Count);
            Tuile Tuile_Swap = new Tuile();
            Tuile_Swap = aP_ListeContenantLaPioche.ElementAt(numeroDeLaTuileRandom);
            aP_ListeContenantLaPioche.RemoveAt(numeroDeLaTuileRandom);
            return Tuile_Swap;
        } //Méthode permettant le swap d'une Tuile, en entrée : la Tuile que le joueur souhaite swaper, en sortit, une nouvelle Tuile random dans les Tuiles restantes, la Tuile en paramètre est ranger dans la pioche 
    }
}
/*      COULEURS 
             * 0 = Rouge 
             * 1 = Orange
             * 2 = Jaune 
             * 3 = Vert 
             * 4 = Bleu
             * 5 = Violet 
             *      FORMES 
             * 0 = Cercle 
             * 1 = Croix 
             * 2 = Losange 
             * 3 = Carré 
             * 4 = Etoile 
             * 5 = Tréfle 
   Nous avons choisis de représenter les Couleurs et Formes des Tuile selon le code suivant 
*/
