﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Partie
    {
        Joueur joueur1;
        Joueur joueur2;
        Joueur joueur3;
        Joueur joueur4;
        Plateau plateaudejeu;
        Pioche piochejeu;
        int nombretours;
        int nombre_joueurs;
        bool finpartie;
        int joueurquijoue;
        public Joueur Joueur1 { get => joueur1; set => joueur1 = value; }
        public Joueur Joueur2 { get => joueur2; set => joueur2 = value; }
        public Joueur Joueur3 { get => joueur3; set => joueur3 = value; }
        public Joueur Joueur4 { get => joueur4; set => joueur4 = value; }
        public Plateau Plateaudejeu { get => plateaudejeu; set => plateaudejeu = value; }
        public Pioche Piochejeu { get => piochejeu; set => piochejeu = value; }
        public int Nombretours { get => nombretours; set => nombretours = value; }
        public int Nombre_joueurs { get => nombre_joueurs; set => nombre_joueurs = value; }
        public bool Finpartie { get => finpartie; set => finpartie = value; }
        public int Joueurquijoue { get => joueurquijoue; set => joueurquijoue = value; }
        public Partie(int largeur,int longueur,Joueur joueur1, Joueur joueur2)
        {
            Joueur1 = joueur1;
            Joueur2 = joueur2;
            plateaudejeu=new Plateau(largeur,longueur);
            Piochejeu = new Pioche();
            nombretours = 0;
            nombre_joueurs = 2;
            finpartie = false;
            joueurquijoue = 1;
        }
        public Partie(int largeur, int longueur, Joueur joueur1, Joueur joueur2,Joueur joueur3)
        {
            Joueur1 = joueur1;
            Joueur2 = joueur2;
            Joueur3 = joueur3;
            plateaudejeu = new Plateau(largeur, longueur);
            Piochejeu = new Pioche();
            nombretours = 0;
            nombre_joueurs = 3;
            finpartie = false;
            joueurquijoue = 1;
        }
        public Partie(int largeur, int longueur, Joueur joueur1, Joueur joueur2,Joueur joueur3,Joueur joueur4)
        {
            Joueur1 = joueur1;
            Joueur2 = joueur2;
            Joueur3 = joueur3;
            Joueur4 = joueur4;
            plateaudejeu = new Plateau(largeur, longueur);
            Piochejeu = new Pioche();
            nombretours = 0;
            nombre_joueurs = 4;
            finpartie = false;
            joueurquijoue = 1;
        }
        public int quiquicomence()
        {
            Random aleatoire = new Random();
            DateTime min;
            Joueur joueurmin=joueur1;
            min=joueur1.GetDate();

            if (joueur2.GetDate() == min)
            {
                if (aleatoire.Next(2) == 0)
                {
                    joueurmin = joueur1;
                }
                else
                {
                    joueurmin = joueur2;
                }
            }
            if (joueur2.GetDate() < min)
            {
                min = joueur2.GetDate();
                joueurmin = joueur2;
            }
            if (nombre_joueurs>2)
            {
                if (Joueur3.GetDate() == min)
                {
                    if (aleatoire.Next(2) == 0)
                    {
                        joueurmin = joueur3;
                    }
                    else
                    {
                        joueurmin = joueur2;
                    }
                }
                if (Joueur3.GetDate() < min)
                {
                    joueurmin = joueur3;
                }
            }
            if (nombre_joueurs > 3)
            {
                if (Joueur4.GetDate() == min)
                {
                    if (aleatoire.Next(2) == 0)
                    {
                        joueurmin = joueur3;
                    }
                    else
                    {
                        joueurmin = joueur4
;
                    }
                }
                if (Joueur4.GetDate() < min)
                {
                    joueurmin = joueur4;
                }
            }

            return joueurmin.Getnum_joueur();
        }
        public bool Veriffinpartie()
        {
            bool flagpartie=true;
            bool flagjoueur;
            for (int nbjoueur=1;nbjoueur<nombre_joueurs+1;nbjoueur++) {
                flagjoueur = true;
                List<Tuile> mainjoueur = convintjoueur(nbjoueur).GetMain();
                for (int index = 0; index < 6; index++)
                {
                    if (mainjoueur.ElementAt(index).GetCouleur() != -1 || mainjoueur.ElementAt(index).GetForme() != -1)
                    {
                        flagpartie = false;
                        flagjoueur = false;
                    }
                }
                if (flagjoueur == true)
                {
                    convintjoueur(nbjoueur).setafini(true);
                }
            }
            return flagpartie;
        }
        public int quiquijoue()
        {
            joueurquijoue++;
            if (joueurquijoue > nombre_joueurs)
            {
                joueurquijoue = 1;
            }
            if (convintjoueur(joueurquijoue).Getafini()==true)
            {
                joueurquijoue++;
            }
            return joueurquijoue;
        }
        public bool fintour()
        {
            quiquijoue();
            //return Veriffinpartie();
            return false;
        }
        public Joueur convintjoueur(int nombre_joueur)
        {
            if (nombre_joueur == 1)
            {
                return joueur1;
            }
            if (nombre_joueur == 2)
            {
                return joueur2;
            }
            if (nombre_joueur == 3)
            {
                return joueur3;
            }
            if (nombre_joueur == 4)
            {
                return joueur4;
            }
            return Joueur1;
        }
    }
}
