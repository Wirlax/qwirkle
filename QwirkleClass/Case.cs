﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleClass
{
    public class Case
    {
        int PositionX;
        int PositionY;
        bool occupe;
        int age;
        Tuile quelletuile;
        public Case(int positionX, int positionY)
        {
            PositionX = positionX;
            PositionY = positionY;
            this.occupe = false;
            age = 0;
        }
        public Case(int positionX, int positionY,Tuile tuileaplacer)
        {
            PositionX = positionX;
            PositionY = positionY;
            occupe = false;
            quelletuile = tuileaplacer;
        }
        public int getPositionX { get => PositionX; set => PositionX = value; }
        public int getPositionY { get => PositionY; set => PositionY = value; }
        public bool getOccupe { get => occupe; set => occupe = value; }
        public Tuile getQuelletuile { get => quelletuile; set => quelletuile = value; }
        public int Age { get => age; set => age = value; }

        public bool verifcaselibre()
        {
            return this.occupe;
        }
        public void settuile(Tuile tuile)
        {
            this.quelletuile = tuile;
        }
        public void Incrementage()
        {
            this.age++;
        }
    }
}
