﻿Public Class Credit

    'Private Sub Credit_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    'End Sub

    'Gestion du bouton retour
    Private Sub BtnRetour_Click(sender As Object, e As EventArgs) Handles BtnRetour.Click
        BtnRetour.BackgroundImage = My.Resources.c_btn_retour_Enfoncer
        timer_Retour.Enabled = True
    End Sub
    Private Sub BtnRetour_MouseHover(sender As Object, e As EventArgs) Handles BtnRetour.MouseHover
        BtnRetour.BackgroundImage = My.Resources.b_btn_retour_sourisAuDessu
    End Sub
    Private Sub BtnRetour_MouseLeave(sender As Object, e As EventArgs) Handles BtnRetour.MouseLeave
        BtnRetour.BackgroundImage = My.Resources.a_btn_retour_nonEnfoncer
    End Sub

    'Timer du btn
    Private Sub timer_Retour_Tick(sender As Object, e As EventArgs) Handles timer_Retour.Tick
        BtnRetour.BackgroundImage = My.Resources.b_btn_retour_sourisAuDessu
        timer_Retour.Enabled = False

        GlobalVariables.song.ClickSong(1, False)

        'On retourne sur l'accueil
        Dim FormMenu As New MenuduJeu
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, FormMenu, Me)
        Me.Hide()
    End Sub

    'Affichage des credits de chacun
    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        AffichageCredit.BackgroundImage = My.Resources.JP_Expl
    End Sub
    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        AffichageCredit.BackgroundImage = My.Resources.SB_Expl
    End Sub
    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        AffichageCredit.BackgroundImage = My.Resources.RT_Expl
    End Sub
    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        AffichageCredit.BackgroundImage = My.Resources.CP_Expl
    End Sub
    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        AffichageCredit.BackgroundImage = My.Resources.VM_Expl
    End Sub
    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        AffichageCredit.BackgroundImage = My.Resources.CH_Expl
    End Sub
    Private Sub Credit_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Lanceur.Close()
    End Sub

End Class