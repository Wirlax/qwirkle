﻿Imports QwirkleClass

Public Class GlobalVariables
    'Variable son qu'on se servira partout
    Public Shared song As LeSon = New LeSon

    'Resolution de l'ecran/des forms, choisi au lancmeent
    Public Shared hauteur As Int16
    Public Shared largeur As Int16
    Public Shared plein_ecran As Boolean


    'Creation des joueur instancier a null
    Public Shared joueur1 As Joueur = Nothing
    Public Shared joueur2 As Joueur = Nothing
    Public Shared joueur3 As Joueur = Nothing
    Public Shared joueur4 As Joueur = Nothing

    'La partie du jeu qui va contenir le plateau, le nb de joueur et pleins d'autres trucs
    Public Shared PartieDeJeu As Partie = Nothing


    'Fonction qui transmet la resolution de forms en forms...
    Public Shared Sub ShowForm(pleinecranouiounon As Boolean, leformcible As Form, leformpere As Form)
        leformcible.Owner = leformpere
        If pleinecranouiounon Then
            leformcible.FormBorderStyle = FormBorderStyle.None
            leformcible.WindowState = FormWindowState.Maximized
        Else
            leformcible.Width = GlobalVariables.largeur
            leformcible.Height = GlobalVariables.hauteur
        End If

        leformcible.Show()
        leformpere.Hide()
    End Sub



    'On renvoie un joueur selon son numero
    Public Shared Function QuelJoueur(nombreDuJoueur As Int16)
        If nombreDuJoueur = 1 Then
            Return joueur1
        ElseIf nombreDuJoueur = 2 Then
            Return joueur2
        ElseIf nombreDuJoueur = 3 Then
            Return joueur3
        Else
            Return joueur4
        End If
    End Function

End Class
