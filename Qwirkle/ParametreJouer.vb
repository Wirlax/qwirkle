﻿Imports QwirkleClass
Public Class ParametreJouer

    'Private Sub ParametreJouer_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    'End Sub

    'Gestion du bouton retour
    Private Sub BtnRetour_Click(sender As Object, e As EventArgs) Handles BtnRetour.Click
        BtnRetour.BackgroundImage = My.Resources.c_btn_retour_Enfoncer
        timer_Retour.Enabled = True

    End Sub
    Private Sub BtnRetour_MouseHover(sender As Object, e As EventArgs) Handles BtnRetour.MouseHover
        BtnRetour.BackgroundImage = My.Resources.b_btn_retour_sourisAuDessu
    End Sub
    Private Sub BtnRetour_MouseLeave(sender As Object, e As EventArgs) Handles BtnRetour.MouseLeave
        BtnRetour.BackgroundImage = My.Resources.a_btn_retour_nonEnfoncer
    End Sub

    'Timer
    Private Sub timer_Retour_Tick(sender As Object, e As EventArgs) Handles timer_Retour.Tick
        BtnRetour.BackgroundImage = My.Resources.b_btn_retour_sourisAuDessu
        timer_Retour.Enabled = False
        GlobalVariables.song.ClickSong(1, False)
        'on retourne a l'accueil

        Dim FormMenu As New MenuduJeu
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, FormMenu, Me)
        Me.Hide()

    End Sub

    'animation' mouse hover & leave -> txt (pas tres utile, juste jolie)
    'txt nbr de joueur
    Private Sub txt_nbrJoueur_MouseHover(sender As Object, e As EventArgs) Handles txt_nbrJoueur.MouseHover
        txt_nbrJoueur.BackgroundImage = My.Resources.nbrJoueur2
    End Sub
    Private Sub txt_nbrJoueur_MouseLeave(sender As Object, e As EventArgs) Handles txt_nbrJoueur.MouseLeave
        txt_nbrJoueur.BackgroundImage = My.Resources.nbrJoueur1
    End Sub

    'txt temps de la aprtie
    Private Sub txt_tempsPartie_MouseHover(sender As Object, e As EventArgs) Handles txt_tempsPartie.MouseHover
        txt_tempsPartie.BackgroundImage = My.Resources.tempspartie2
    End Sub
    Private Sub txt_tempsPartie_MouseLeave(sender As Object, e As EventArgs) Handles txt_tempsPartie.MouseLeave
        txt_tempsPartie.BackgroundImage = My.Resources.tempspartie1
    End Sub

    'Btn Letsgo
    Private Sub BtnLetsGo_Click(sender As Object, e As EventArgs) Handles BtnLetsGo.Click
        BtnLetsGo.BackgroundImage = My.Resources.c_btn_letsgo_Enfoncer
        timer_LetsGo.Enabled = True

        'Les variables
        'On stock La valeur nombre d ejoueur issu du form dans une variable pour la manipuler
        Dim nbreJoueur = nbrJoueur.Value

        'Change rien pour le moment
        Dim LaDureeDeLaPartie As Int16 = tempsPartie.Value

        'Creation des joueurs
        'Joueur(String pseudo, Date date_nais)
        If nbreJoueur >= 2 Then
            GlobalVariables.joueur1 = New Joueur(PseudoJoueur1.Text, ddnj1.Value, 1)
            GlobalVariables.joueur2 = New Joueur(PseudoJoueur2.Text, ddnj2.Value, 2)
        End If
        If nbreJoueur >= 3 Then
            GlobalVariables.joueur3 = New Joueur(PseudoJoueur3.Text, ddnj3.Value, 3)
        End If
        If nbreJoueur >= 4 Then
            GlobalVariables.joueur4 = New Joueur(PseudoJoueur4.Text, ddnj4.Value, 4)
        End If


        'Creation d ela partie en fonction de la nulliter des joueur (si ils sont cree ou non, depend du nombre de joueur juste au dessu)
        If GlobalVariables.joueur3 IsNot Nothing Then
            If GlobalVariables.joueur4 IsNot Nothing Then
                GlobalVariables.PartieDeJeu = New Partie(30, 30, GlobalVariables.joueur1, GlobalVariables.joueur2, GlobalVariables.joueur3, GlobalVariables.joueur4)
            Else
                GlobalVariables.PartieDeJeu = New Partie(30, 30, GlobalVariables.joueur1, GlobalVariables.joueur2, GlobalVariables.joueur3)
            End If
        Else
            GlobalVariables.PartieDeJeu = New Partie(30, 30, GlobalVariables.joueur1, GlobalVariables.joueur2)
        End If

    End Sub
    Private Sub BtnLetsGo_MouseHover(sender As Object, e As EventArgs) Handles BtnLetsGo.MouseHover
        BtnLetsGo.BackgroundImage = My.Resources.b_btn_letsgo_sourisAuDessu
    End Sub
    Private Sub BtnLetsGo_MouseLeave(sender As Object, e As EventArgs) Handles BtnLetsGo.MouseLeave
        BtnLetsGo.BackgroundImage = My.Resources.a_btn_letsgo_nonEnfoncer
    End Sub

    'timer letsgo
    Private Sub timer_LetsGo_Tick(sender As Object, e As EventArgs) Handles timer_LetsGo.Tick
        timer_LetsGo.Enabled = False
        BtnLetsGo.BackgroundImage = My.Resources.b_btn_letsgo_sourisAuDessu
        GlobalVariables.song.ClickSong(1, False)
        'on ouvre le jeu apres verif des informations
        '....
        Dim Form As New PlateauDeJeu
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, Form, Me)
    End Sub

    Private Sub nbrJoueur_ValueChanged(sender As Object, e As EventArgs) Handles nbrJoueur.ValueChanged
        If nbrJoueur.Value = 2 Then
            PannelJoueur3.Enabled = False
            pannelJoueur4.Enabled = False
        End If
        If nbrJoueur.Value = 3 Then
            pannelJoueur4.Enabled = False
            PannelJoueur3.Enabled = True
        End If
        If nbrJoueur.Value = 4 Then
            pannelJoueur4.Enabled = True
        End If
    End Sub

    Private Sub ParametreJouer_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Lanceur.Close()
    End Sub

    Private Sub ParametreJouer_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class