﻿Imports System.IO
'Imports WMPLib

'creation d'une classe son pour pouvoir l'inclure dans toutes les pages, ca evite de copier coller la fonction playSong dans chaqueq forms
Public Class LeSon

    Dim path As String
    Dim Mute As Boolean

    Public Function getMute() As Boolean
        Return Mute
    End Function

    Public Sub ClickSong(ByVal numero As Int16, ByVal boucle As Boolean)
        If Mute = False Then
            If numero = 1 Then
                My.Computer.Audio.Play(My.Resources.clickNormalWAV, AudioPlayMode.WaitToComplete)
                'My.Computer.Audio.Play("C:\Users\Harderue\Source\Repos\qwirkle\Qwirkle\Resources\clickNormalWAV.wav", AudioPlayMode.WaitToComplete)
            End If
            If numero = 2 Then
                My.Computer.Audio.Play(My.Resources.clickQuitterWAV, AudioPlayMode.WaitToComplete)
                'My.Computer.Audio.Play("C:\Users\Harderue\Source\Repos\qwirkle\Qwirkle\Resources\clickQuitterWAV.wav", AudioPlayMode.WaitToComplete)
            End If
        End If
    End Sub

    'Dim WithEvents junkBox As WMPLib.WindowsMediaPlayer

    'constru en vb
    Public Sub New()
        Me.Mute = False
        'junkBox = New WMPLib.WindowsMediaPlayer
    End Sub

    'Public Sub playSong(ByVal url As String, ByVal boucle As Boolean)
    '    junkBox.URL = url
    '    'donne la possibiliter d ejouer un son en boucle, pratique pour les son d'ambiance
    '    If boucle = True Then
    '        junkBox.settings.setMode("loop", True)
    '    End If
    '    junkBox.controls.play()
    'End Sub

    ''comme un get en c#
    'Function estIlMute() As Boolean
    '    If mute = True Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    'petite fonction pour plus tard si on veut donner la possibiliter au joueur de couper le son
    'reste a decider si je veux juste couper le volume ou carement arreter la musique

    'semblable a une fonctin set en c#
    Public Sub onMuteLeSon(ByVal ouiOuNon As Boolean)
        If ouiOuNon = True Then
            'junkBox.settings.volume = 0
            Mute = True
            'junkBox.controls.stop()
        End If
        If ouiOuNon = False Then
            'junkBox.settings.volume = 100
            Mute = False
            'junkBox.controls.play()
        End If
    End Sub

End Class
