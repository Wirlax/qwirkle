﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MenuduJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.timer_Jouer = New System.Windows.Forms.Timer(Me.components)
        Me.timer_Regles = New System.Windows.Forms.Timer(Me.components)
        Me.timer_Quitter = New System.Windows.Forms.Timer(Me.components)
        Me.timer_Credit = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnQuitter = New System.Windows.Forms.PictureBox()
        Me.BtnRegles = New System.Windows.Forms.PictureBox()
        Me.BtnJouer = New System.Windows.Forms.PictureBox()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.BtnCredit = New System.Windows.Forms.PictureBox()
        Me.song_o_f = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.BtnQuitter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnRegles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnJouer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnCredit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.song_o_f, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'timer_Jouer
        '
        '
        'timer_Regles
        '
        '
        'timer_Quitter
        '
        '
        'timer_Credit
        '
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.Controls.Add(Me.BtnQuitter, 1, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.BtnRegles, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.BtnJouer, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Logo, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.BtnCredit, 2, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.song_o_f, 0, 9)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 10
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.720351!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.23573!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.892787!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.59282!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.430498!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.59647!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.430498!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.59647!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 1.430498!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.07387!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1600, 886)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'BtnQuitter
        '
        Me.BtnQuitter.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnQuitter.BackColor = System.Drawing.Color.Transparent
        Me.BtnQuitter.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_quitter_nonEnfoncer
        Me.BtnQuitter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnQuitter.Location = New System.Drawing.Point(683, 576)
        Me.BtnQuitter.Name = "BtnQuitter"
        Me.BtnQuitter.Size = New System.Drawing.Size(233, 154)
        Me.BtnQuitter.TabIndex = 7
        Me.BtnQuitter.TabStop = False
        '
        'BtnRegles
        '
        Me.BtnRegles.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnRegles.BackColor = System.Drawing.Color.Transparent
        Me.BtnRegles.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_regles_nonEnfoncer
        Me.BtnRegles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnRegles.Location = New System.Drawing.Point(683, 400)
        Me.BtnRegles.Name = "BtnRegles"
        Me.BtnRegles.Size = New System.Drawing.Size(233, 154)
        Me.BtnRegles.TabIndex = 6
        Me.BtnRegles.TabStop = False
        '
        'BtnJouer
        '
        Me.BtnJouer.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.BtnJouer.BackColor = System.Drawing.Color.Transparent
        Me.BtnJouer.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_jouer_nonEnfoncer
        Me.BtnJouer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnJouer.Location = New System.Drawing.Point(683, 224)
        Me.BtnJouer.Name = "BtnJouer"
        Me.BtnJouer.Size = New System.Drawing.Size(233, 154)
        Me.BtnJouer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.BtnJouer.TabIndex = 5
        Me.BtnJouer.TabStop = False
        '
        'Logo
        '
        Me.Logo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Logo.BackColor = System.Drawing.Color.Transparent
        Me.Logo.BackgroundImage = Global.Qwirkle.My.Resources.Resources.LOGO
        Me.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Logo.Location = New System.Drawing.Point(536, 18)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(527, 155)
        Me.Logo.TabIndex = 9
        Me.Logo.TabStop = False
        '
        'BtnCredit
        '
        Me.BtnCredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnCredit.BackColor = System.Drawing.Color.Transparent
        Me.BtnCredit.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_credit_nonEnfoncer
        Me.BtnCredit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnCredit.Location = New System.Drawing.Point(1470, 756)
        Me.BtnCredit.Margin = New System.Windows.Forms.Padding(0, 0, 30, 30)
        Me.BtnCredit.Name = "BtnCredit"
        Me.BtnCredit.Size = New System.Drawing.Size(100, 100)
        Me.BtnCredit.TabIndex = 8
        Me.BtnCredit.TabStop = False
        '
        'song_o_f
        '
        Me.song_o_f.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.song_o_f.BackColor = System.Drawing.Color.Transparent
        Me.song_o_f.BackgroundImage = Global.Qwirkle.My.Resources.Resources.sicOn
        Me.song_o_f.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.song_o_f.Location = New System.Drawing.Point(30, 781)
        Me.song_o_f.Margin = New System.Windows.Forms.Padding(30, 3, 3, 30)
        Me.song_o_f.Name = "song_o_f"
        Me.song_o_f.Size = New System.Drawing.Size(75, 75)
        Me.song_o_f.TabIndex = 10
        Me.song_o_f.TabStop = False
        '
        'MenuduJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(1600, 886)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "MenuduJeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu Qwirkle"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.BtnQuitter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnRegles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnJouer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnCredit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.song_o_f, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents BtnJouer As PictureBox
    Friend WithEvents BtnRegles As PictureBox
    Friend WithEvents BtnQuitter As PictureBox
    Friend WithEvents BtnCredit As PictureBox
    Friend WithEvents Logo As PictureBox
    Friend WithEvents timer_Jouer As Timer
    Friend WithEvents timer_Regles As Timer
    Friend WithEvents timer_Quitter As Timer
    Friend WithEvents timer_Credit As Timer
    Friend WithEvents song_o_f As PictureBox
End Class
