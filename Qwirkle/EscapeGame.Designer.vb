﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EscapeGame
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnResume = New System.Windows.Forms.PictureBox()
        Me.btnQuitter = New System.Windows.Forms.PictureBox()
        Me.btnMusique = New System.Windows.Forms.PictureBox()
        Me.Resume_timer = New System.Windows.Forms.Timer(Me.components)
        Me.Quitter_timer = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.btnResume, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnQuitter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnMusique, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.42959!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.57041!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(876, 863)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.BackgroundImage = Global.Qwirkle.My.Resources.Resources.LOGO
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(250, 6)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(375, 154)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334!))
        Me.TableLayoutPanel2.Controls.Add(Me.btnResume, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.btnQuitter, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.btnMusique, 0, 4)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(4, 172)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(868, 686)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'btnResume
        '
        Me.btnResume.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnResume.BackColor = System.Drawing.Color.Transparent
        Me.btnResume.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_resume_nonEnfoncer
        Me.btnResume.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnResume.Location = New System.Drawing.Point(321, 148)
        Me.btnResume.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnResume.Name = "btnResume"
        Me.btnResume.Size = New System.Drawing.Size(225, 115)
        Me.btnResume.TabIndex = 0
        Me.btnResume.TabStop = False
        '
        'btnQuitter
        '
        Me.btnQuitter.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnQuitter.BackColor = System.Drawing.Color.Transparent
        Me.btnQuitter.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_quitter_nonEnfoncer
        Me.btnQuitter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnQuitter.Location = New System.Drawing.Point(321, 285)
        Me.btnQuitter.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnQuitter.Name = "btnQuitter"
        Me.btnQuitter.Size = New System.Drawing.Size(225, 115)
        Me.btnQuitter.TabIndex = 1
        Me.btnQuitter.TabStop = False
        '
        'btnMusique
        '
        Me.btnMusique.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.btnMusique.BackColor = System.Drawing.Color.Transparent
        Me.btnMusique.BackgroundImage = Global.Qwirkle.My.Resources.Resources.sicOn
        Me.btnMusique.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMusique.Location = New System.Drawing.Point(30, 559)
        Me.btnMusique.Margin = New System.Windows.Forms.Padding(30, 5, 4, 5)
        Me.btnMusique.Name = "btnMusique"
        Me.btnMusique.Size = New System.Drawing.Size(112, 115)
        Me.btnMusique.TabIndex = 2
        Me.btnMusique.TabStop = False
        '
        'Resume_timer
        '
        '
        'Quitter_timer
        '
        '
        'EscapeGame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(876, 863)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "EscapeGame"
        Me.Text = "Parametre"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.btnResume, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnQuitter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnMusique, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents btnResume As PictureBox
    Friend WithEvents btnQuitter As PictureBox
    Friend WithEvents btnMusique As PictureBox
    Friend WithEvents Resume_timer As Timer
    Friend WithEvents Quitter_timer As Timer
End Class
