﻿Imports QwirkleClass

Public Class PlateauDeJeu

    Dim differnceX As Int16 = 0
    Dim differnceY As Int16 = 0
    Dim numero_tuile As Int32 = 0
    Dim TableauVerifTout(2) As Int32
    Dim JoueurQuiJoue As Int16
    Dim tableaucouleurforme(2) As Int32
    'Taille Plateau pour le vb
    Dim nombre_column As Int16
    Dim nombre_row As Int16
    Dim LeTLPplateau As TableLayoutPanel = New TableLayoutPanel


    Dim listtuile As List(Of Tuile)
    Dim njoueur As Joueur


    Dim joueursco As Int16

    Dim Swap As Boolean = False
    Dim peutplusswap As Boolean = False

    'load
    Private Sub PlateauDeJeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        BtnSwap.AllowDrop = True

        'Affichage des scores ect sur la gauche du plateau selon le nbr de joueur

        LabelPseudoJ1.Text = GlobalVariables.joueur1.GetPseudo()
        LabelPseudoJ2.Text = GlobalVariables.joueur2.GetPseudo()
        If GlobalVariables.PartieDeJeu.Nombre_joueurs >= 3 Then
            LabelPseudoJ3.Visible = True
            Scorej3.Visible = True
            LabelPseudoJ3.Text = GlobalVariables.joueur3.GetPseudo()
        Else
            LabelPseudoJ3.Visible = False
            Scorej3.Visible = False
        End If
        If GlobalVariables.PartieDeJeu.Nombre_joueurs = 4 Then
            LabelPseudoJ4.Visible = True
            Scorej4.Visible = True
            LabelPseudoJ4.Text = GlobalVariables.joueur4.GetPseudo()
        Else
            LabelPseudoJ4.Visible = False
            Scorej4.Visible = False
        End If

        'Si le nombre de tour = 0, alors on compare les sates de naissance pour savoir qui commence
        If GlobalVariables.PartieDeJeu.Nombretours = 0 Then
            JoueurQuiJoue = GlobalVariables.PartieDeJeu.quiquicomence
        End If
        'Le plateau est desormais dans la classe partie.
        'GlobalVariables.PartieDeJeu.Plateaudejeu...

        nombre_row = 10
        nombre_column = 10
        TableLayoutPanel1.Controls.Add(LeTLPplateau, 1, 0)
        LeTLPplateau.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single
        TableLayoutPanel1.SuspendLayout()
        LeTLPplateau.SuspendLayout()
        LeTLPplateau.AutoSize = True
        LeTLPplateau.AutoScroll = True
        LeTLPplateau.Anchor = AnchorStyles.Bottom And AnchorStyles.Left And AnchorStyles.Right And AnchorStyles.Top
        differnceX = (GlobalVariables.PartieDeJeu.Plateaudejeu.Longueur / 2)
        differnceY = (GlobalVariables.PartieDeJeu.Plateaudejeu.Largeur / 2)


        'Création du tableau de base
        For i = 0 To nombre_column
            LeTLPplateau.ColumnCount += 1
            For x = 0 To nombre_row
                LeTLPplateau.RowCount += 1
                Dim pic As PictureBox = New PictureBox
                pic.AllowDrop = True
                Dim taille As New Size(50, 50)
                pic.Size = taille
                pic.BackgroundImageLayout = ImageLayout.Stretch
                AddHandler pic.DragDrop, AddressOf pic_DragDrop
                AddHandler pic.DragEnter, AddressOf pic_DragEnter
                LeTLPplateau.Controls.Add(pic, i, x)
            Next
        Next
        LeTLPplateau.ResumeLayout()
        TableLayoutPanel1.ResumeLayout()

        'Set des mains des joueurs en fonction du  nombre de joueurs 
        If GlobalVariables.PartieDeJeu.Nombre_joueurs = 2 Then
            GlobalVariables.joueur1.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            GlobalVariables.joueur2.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
        ElseIf GlobalVariables.PartieDeJeu.Nombre_joueurs = 3 Then
            GlobalVariables.joueur1.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            GlobalVariables.joueur2.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            GlobalVariables.joueur3.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            joueursco = 3
        ElseIf GlobalVariables.PartieDeJeu.Nombre_joueurs = 4 Then
            GlobalVariables.joueur1.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            GlobalVariables.joueur2.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            GlobalVariables.joueur3.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            GlobalVariables.joueur4.SetMain(GlobalVariables.PartieDeJeu.Piochejeu)
            joueursco = 4
        End If





        njoueur = GlobalVariables.QuelJoueur(GlobalVariables.PartieDeJeu.quiquicomence())
        'Surbrillance du joueur qui commence
        If GlobalVariables.PartieDeJeu.quiquicomence() = 1 Then
            LabelPseudoJ1.BackColor = Color.BurlyWood
        ElseIf GlobalVariables.PartieDeJeu.quiquicomence() = 2 Then
            LabelPseudoJ2.BackColor = Color.BurlyWood
        ElseIf GlobalVariables.PartieDeJeu.quiquicomence() = 3 Then
            LabelPseudoJ3.BackColor = Color.BurlyWood
        Else
            LabelPseudoJ4.BackColor = Color.BurlyWood
        End If

        'Affichage Nombre de tuiles 
        NbrTuilejoueurActu.Text = GlobalVariables.PartieDeJeu.Piochejeu.Get_Count_Liste()

        'Affichage de la main du joueur qui commence
        listtuile = njoueur.GetMain()
        TableLayoutPanel1.ResumeLayout()
        TableLayoutPanel4.SuspendLayout()
        For i = 0 To 5
            Dim pic As PictureBox
            pic = TableLayoutPanel4.GetControlFromPosition(i, 0)
            pic.Name = Tuileversnom(listtuile(i))
            pic.BackgroundImage = Image.FromFile(Tuileversbackgroundimage(listtuile(i)))
        Next
        TableLayoutPanel4.ResumeLayout()
        TableLayoutPanel1.ResumeLayout()
    End Sub

    'BTN swap
    Private Sub BtnSwap_Click(sender As Object, e As EventArgs) Handles BtnSwap.Click
        Swap_timer.Enabled = True
        BtnSwap.BackgroundImage = My.Resources.c_btn_swap_Enfoncer
    End Sub
    Private Sub BtnSwap_MouseHover(sender As Object, e As EventArgs) Handles BtnSwap.MouseHover
        BtnSwap.BackgroundImage = My.Resources.b_btn_swap_sourisAuDessu
    End Sub
    Private Sub BtnSwap_MouseLeave(sender As Object, e As EventArgs) Handles BtnSwap.MouseLeave
        BtnSwap.BackgroundImage = My.Resources.a_btn_swap_nonEnfoncer
    End Sub


    'BTN Fin tour
    Private Sub BtnFinTour_Click(sender As Object, e As EventArgs) Handles BtnFinTour.Click
        FinTour_timer.Enabled = True
        BtnFinTour.BackgroundImage = My.Resources.btn_FinTour_Enfoncer
        Scorej1.Text = GlobalVariables.joueur1.GetScore()
        Scorej2.Text = GlobalVariables.joueur2.GetScore()
        If Joueursco = 3 Then
            Scorej3.Text = GlobalVariables.joueur3.GetScore()
        End If
        If Joueursco = 4 Then
            Scorej4.Text = GlobalVariables.joueur4.GetScore()
        End If

    End Sub
    Private Sub BtnFinTour_MouseHover(sender As Object, e As EventArgs) Handles BtnFinTour.MouseHover
        BtnFinTour.BackgroundImage = My.Resources.btn_FinTour_sourisAuDessu
    End Sub
    Private Sub BtnFinTour_MouseLeave(sender As Object, e As EventArgs) Handles BtnFinTour.MouseLeave
        BtnFinTour.BackgroundImage = My.Resources.btn_FinTour_nonEnfoncer
    End Sub

    'On ouvre un form parametre pour soit quitter, soit eteindre le son.
    Private Sub PlateauDeJeu_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            EscapeGame.ShowDialog()
        End If
    End Sub

    'timer
    Private Sub Swap_timer_Tick(sender As Object, e As EventArgs) Handles Swap_timer.Tick
        Swap_timer.Enabled = False
        BtnSwap.BackgroundImage = My.Resources.b_btn_swap_sourisAuDessu

        GlobalVariables.song.ClickSong(1, False)

        'Appel de des fonction gerant le swap en csharp...




    End Sub

    Private Sub FinTour_timer_Tick(sender As Object, e As EventArgs) Handles FinTour_timer.Tick
        FinTour_timer.Enabled = False

        BtnFinTour.BackgroundImage = My.Resources.btn_FinTour_sourisAuDessu

        GlobalVariables.song.ClickSong(1, False)

        'changement de couleur du texte "nbr tuiles" selon le joueur qui doit jouer
        'Appel de la fonction fin tour csharp...
        'appel de la fonction qui verifie le nombre de tuile de chaque joueur et qui appelera le form finPartie qui affichera les resultats

        'On dit qui va jouer au prochain tour et on met son backcolor en marron chelou
        JoueurQuiJoue = GlobalVariables.PartieDeJeu.quiquijoue
        Dim j As Joueur = GlobalVariables.QuelJoueur(JoueurQuiJoue)
        Dim nbJ As Int16 = GlobalVariables.PartieDeJeu.Nombre_joueurs


        If nbJ >= 2 Then
            If j Is GlobalVariables.joueur1 Then
                LabelPseudoJ1.BackColor = Color.BurlyWood
                LabelPseudoJ2.BackColor = Nothing
                LabelPseudoJ3.BackColor = Nothing
                LabelPseudoJ4.BackColor = Nothing
            Else
                LabelPseudoJ1.BackColor = Nothing
                LabelPseudoJ2.BackColor = Color.BurlyWood
                LabelPseudoJ3.BackColor = Nothing
                LabelPseudoJ4.BackColor = Nothing
            End If
        End If

        If nbJ = 3 Then
            If j Is GlobalVariables.joueur3 Then
                LabelPseudoJ1.BackColor = Nothing
                LabelPseudoJ2.BackColor = Nothing
                LabelPseudoJ3.BackColor = Color.BurlyWood
                LabelPseudoJ4.BackColor = Nothing
            End If
        End If

        If nbJ = 4 Then
            If j Is GlobalVariables.joueur3 Then
                LabelPseudoJ1.BackColor = Nothing
                LabelPseudoJ2.BackColor = Nothing
                LabelPseudoJ3.BackColor = Nothing
                LabelPseudoJ4.BackColor = Color.BurlyWood
            End If
        End If

        'Fin de l'affichage du marron chelou

        'Si partie terminé message
        If GlobalVariables.PartieDeJeu.fintour() = True Then
            FinPartie.ShowDialog()
        End If

        'Changement de joueur et affichage de sa main
        njoueur.Remise_fin_de_tour(njoueur.VerifNbPieceManquante(), GlobalVariables.PartieDeJeu.Piochejeu)
        njoueur = GlobalVariables.QuelJoueur(GlobalVariables.PartieDeJeu.quiquijoue())
        listtuile = njoueur.GetMain()
        TableLayoutPanel1.SuspendLayout()
        TableLayoutPanel4.SuspendLayout()
        Dim nbpieces As Int16 = njoueur.GetNbPieceMain() - 1
        For i = 0 To nbpieces
            Dim pic As PictureBox = TableLayoutPanel4.GetControlFromPosition(i, 0)
            pic.Name = Tuileversnom(listtuile(i))
            pic.BackgroundImage = Image.FromFile(Tuileversbackgroundimage(listtuile(i)))
        Next
        TableLayoutPanel4.ResumeLayout()
        TableLayoutPanel1.ResumeLayout()

        'Actualisation du nombre de tuiles restantes
        NbrTuilejoueurActu.Text = GlobalVariables.PartieDeJeu.Piochejeu.Get_Count_Liste()

        'remise à zéro des drapeaux swap et peutplusswap
        Swap = False
        peutplusswap = False
    End Sub
    'Fermeture jeu
    Private Sub PlateauDeJeu_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Lanceur.Close()
    End Sub

    Private Sub pic_DragEnter(sender As Object, e As DragEventArgs)
        Dim pos As TableLayoutPanelCellPosition = LeTLPplateau.GetCellPosition(sender)
        Dim posx As Int16
        Dim posy As Int16
        posx = pos.Column
        posy = pos.Row

        TableauVerifTout = GlobalVariables.PartieDeJeu.Plateaudejeu.Veriftout(posx, posy, differnceX, differnceY, tableaucouleurforme(0), tableaucouleurforme(1))
        'TableauVerifTout(1) = 1 ou 0 (tout est bon ou non)
        'TableauVerifTout(2) = Le score du joueur




        'On cree une variable joueur qui va stocker le joueur qui joue
        Dim JoueurJouant As Joueur
        'On lui donne la valeur du joueur qui doit jouer, avec la fonction dans GlobalVariable qui retourne un joueur en fonction du numero de joueur passer
        JoueurJouant = GlobalVariables.QuelJoueur(JoueurQuiJoue)

        'On ajoute le score a ce joueur
        JoueurJouant.AddScore(TableauVerifTout(1))


        'On verifie que VerifTout(1) = 1, ce qui veut dire que tous est ok, sinon on place pas la tuile.
        If TableauVerifTout(0) = 1 And Swap = False Then
            If e.Data.GetDataPresent(DataFormats.Bitmap) = True Then
                e.Effect = DragDropEffects.Move
            End If
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub pic_DragDrop(sender As Object, e As DragEventArgs)
        Dim pos As TableLayoutPanelCellPosition = LeTLPplateau.GetCellPosition(sender)
        Dim posx As Int16
        Dim posy As Int16
        posx = pos.Column
        posy = pos.Row
        GlobalVariables.PartieDeJeu.Plateaudejeu.Placetuile(posx, posy, differnceX, differnceY, New Tuile(tableaucouleurforme(0), tableaucouleurforme(1)))
        'Savoir si il faut rajouter ligne ou colonne en fonction de la position 
        If posx = 0 Then
            ColumnAjoutgauche()
            differnceX -= 1
        ElseIf posx = nombre_column Then
            ColumnAjout()
        End If
        If posy = 0 Then
            RowAjouthaut()
            differnceY -= 1
        ElseIf posy = nombre_row Then
            RowAjout()
        End If
        sender.BackgroundImage = e.Data.GetData(DataFormats.Bitmap)
        'placement dans le tableau donc peut plus swap de pièces
        peutplusswap = True
    End Sub

    Private Sub Pioche1_MouseMove(sender As Object, e As MouseEventArgs) Handles Pioche6.MouseMove, Piece_3_1_2.MouseMove, Piece_4_4.MouseMove, Piece_4_1.MouseMove, Piece_3_1_1.MouseMove, Piece_3_4.MouseMove
        Dim TuileMain As PictureBox = sender
        Dim rep As DragDropEffects
        If e.Button = MouseButtons.Left Then
            tableaucouleurforme = imageverslobjet(sender)
            'pour pas pouvoir draganddrop une tuile vide
            If TuileMain.BackgroundImage IsNot Nothing Then
                rep = TuileMain.DoDragDrop(TuileMain.BackgroundImage, DragDropEffects.Move)
            End If


            If (rep = DragDropEffects.Move) Then
                Dim placecolumn As Int16
                Dim place As TableLayoutPanelCellPosition
                place = TableLayoutPanel4.GetCellPosition(sender)
                placecolumn = place.Column
                TuileMain.BackgroundImage = Nothing
                'si le joueur n'a pas commencé à swap
                If Swap = False Then

                    listtuile.RemoveAt(placecolumn)

                    Dim nbpieces As Int16 = njoueur.GetNbPieceMain() - 1
                    For i = 0 To 5
                        Dim pic As PictureBox = TableLayoutPanel4.GetControlFromPosition(i, 0)
                        pic.BackgroundImage = Nothing
                    Next
                    For i = 0 To nbpieces
                        Dim pic As PictureBox = TableLayoutPanel4.GetControlFromPosition(i, 0)
                        pic.Name = Tuileversnom(listtuile(i))
                        pic.BackgroundImage = Image.FromFile(Tuileversbackgroundimage(listtuile(i)))
                    Next

                Else
                    'Si le joueur à commencé à swap
                    njoueur.Swap_Tuile(placecolumn, GlobalVariables.PartieDeJeu.Piochejeu)
                End If

            End If
        End If
    End Sub
    'Ajout d'une ligne en bas et ajustement du tableau et des pictures box
    Private Sub RowAjout()
        Dim r As New RowStyle
        r.SizeType = SizeType.AutoSize
        TableLayoutPanel1.SuspendLayout()
        LeTLPplateau.SuspendLayout()
        LeTLPplateau.RowStyles.Add(r)
        LeTLPplateau.RowCount += 1
        nombre_row += 1
        For x = 0 To nombre_column
            Dim pic As New PictureBox
            pic.AllowDrop = True
            Dim taille As New Size(50, 50)
            pic.Size = taille
            pic.BackgroundImageLayout = ImageLayout.Stretch
            AddHandler pic.DragDrop, AddressOf pic_DragDrop
            AddHandler pic.DragEnter, AddressOf pic_DragEnter
            LeTLPplateau.Controls.Add(pic, x, nombre_row)
        Next
        TableLayoutPanel1.ResumeLayout()
        LeTLPplateau.ResumeLayout()
    End Sub
    'Ajout d'une colonne à gauche et ajustement du tableau et des pictures box
    Private Sub ColumnAjout()
        Dim r As New ColumnStyle
        r.SizeType = SizeType.AutoSize
        TableLayoutPanel1.SuspendLayout()
        LeTLPplateau.SuspendLayout()
        LeTLPplateau.ColumnStyles.Add(r)
        LeTLPplateau.ColumnCount += 1
        nombre_column += 1
        For x = 0 To nombre_row
            Dim pic As New PictureBox
            pic.AllowDrop = True
            Dim taille As New Size(50, 50)
            pic.Size = taille
            pic.BackgroundImageLayout = ImageLayout.Stretch
            AddHandler pic.DragDrop, AddressOf pic_DragDrop
            AddHandler pic.DragEnter, AddressOf pic_DragEnter
            LeTLPplateau.Controls.Add(pic, nombre_column, x)
        Next
        TableLayoutPanel1.ResumeLayout()
        LeTLPplateau.ResumeLayout()
    End Sub

    'Ajout d'une colonne à gauche changement de place des pièces en ajoutant une colonne à droite et en libérant l'espace à gauche
    Private Sub ColumnAjoutgauche()
        Dim r As New ColumnStyle
        r.SizeType = SizeType.AutoSize
        TableLayoutPanel1.SuspendLayout()
        LeTLPplateau.SuspendLayout()
        LeTLPplateau.ColumnStyles.Add(r)
        LeTLPplateau.ColumnCount += 1
        nombre_column += 1
        For i = (nombre_column - 1) To 0 Step -1
            For j = 0 To nombre_row
                Dim pic As PictureBox = LeTLPplateau.GetControlFromPosition(i, j)
                LeTLPplateau.Controls.Add(pic, i + 1, j)
            Next
        Next
        For i = 0 To nombre_row
            Dim pic As New PictureBox
            pic.AllowDrop = True
            Dim taille As New Size(50, 50)
            pic.Size = taille
            pic.BackgroundImageLayout = ImageLayout.Stretch
            AddHandler pic.DragDrop, AddressOf pic_DragDrop
            AddHandler pic.DragEnter, AddressOf pic_DragEnter
            LeTLPplateau.Controls.Add(pic, 0, i)
        Next
        TableLayoutPanel1.ResumeLayout()
        LeTLPplateau.ResumeLayout()
    End Sub
    'Ajout d'une ligne en haut changement de place des pièces en ajoutant une ligne en bas et en libérant l'espace en haut
    Private Sub RowAjouthaut()
        Dim r As New RowStyle
        r.SizeType = SizeType.AutoSize
        TableLayoutPanel1.SuspendLayout()
        LeTLPplateau.SuspendLayout()
        LeTLPplateau.RowStyles.Add(r)
        LeTLPplateau.RowCount += 1
        nombre_row += 1
        For i = (nombre_row - 1) To 0 Step -1
            For j = 0 To nombre_column
                Dim pic As PictureBox = LeTLPplateau.GetControlFromPosition(j, i)
                LeTLPplateau.Controls.Add(pic, j, i + 1)
            Next
        Next
        For i = 0 To nombre_column
            Dim pic As New PictureBox
            pic.AllowDrop = True
            Dim taille As New Size(50, 50)
            pic.Size = taille
            pic.BackgroundImageLayout = ImageLayout.Stretch
            AddHandler pic.DragDrop, AddressOf pic_DragDrop
            AddHandler pic.DragEnter, AddressOf pic_DragEnter
            LeTLPplateau.Controls.Add(pic, i, 0)
        Next
        TableLayoutPanel1.ResumeLayout()
        LeTLPplateau.ResumeLayout()
    End Sub

    'Transformation d'une image en un objet tuile 
    Private Function imageverslobjet(lesender As PictureBox)

        Dim stringreader, f, c As String
        stringreader = lesender.Name
        Dim Words As String() = stringreader.Split(New Char() {"_"})
        f = Words(1)
        c = Words(2)
        Dim tabretour(2) As Int32
        tabretour(0) = c
        tabretour(1) = f
        Return tabretour
    End Function

    'transformation d'une tuile en un nom d'image 
    Private Function Tuileversnom(lesender As Tuile)
        Dim nomtuile, f, c, string_tuile As String
        f = lesender.GetForme()
        c = lesender.GetCouleur()
        string_tuile = numero_tuile
        nomtuile = "piece_" + f + "_" + c + "_" + string_tuile
        numero_tuile += 1
        Return nomtuile
    End Function
    'transformation d'une tuile vers un chemin backgroundimage
    Private Function Tuileversbackgroundimage(lesender As Tuile)
        Dim f, c As String
        f = lesender.GetForme()
        c = lesender.GetCouleur()
        Dim path As String
        Dim i, y As Int64
        Dim words As String()
        words = Application.StartupPath.Split(New Char() {"\"})
        i = 0
        path = ""
        While words(i) <> "bin"
            i += 1
        End While
        path = words(0)
        For y = 1 To i - 1
            path = path + "\" + words(y)
        Next
        path = path + "\Resources\piece_" + f + "_" + c + ".png"
        Return path
    End Function

    'Dragenter du swap
    Private Sub BtnSwap_DragEnter(sender As Object, e As DragEventArgs) Handles BtnSwap.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) And peutplusswap = False Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    'dragdrop du swap
    Private Sub BtnSwap_DragDrop(sender As Object, e As DragEventArgs) Handles BtnSwap.DragDrop
        sender.BackgroundImage = e.Data.GetData(DataFormats.Bitmap)
        'swap vrai donc peut plus placer de tuile dans le tableau
        Swap = True
    End Sub
End Class