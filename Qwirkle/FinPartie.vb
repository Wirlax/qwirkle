﻿Public Class FinPartie
    Private Sub FinPartie_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'on donne le pseudo de celui qui a le plus gros score, qui a gagner la partie.
        'Pseudogagnant.Text = 

        'On stock le nbr de joueur de l'objet partie dans une variable
        Dim nbrJoueruPartie = GlobalVariables.PartieDeJeu.Nombre_joueurs

        'On affiche le nombre de tour qu'a durer la partie
        nbrTourP.Text = GlobalVariables.PartieDeJeu.Nombretours

        'On donne le pseudo du gagant et du deuxieme
        'Pseudo1er.Text = Pseudogagnant.Text
        'Pseudo2em.Text = GlobalVariables.

        'On donne leurs scores
        'Score1er.Text = GlobalVariables.
        'Score2em.Text = GlobalVariables.


        If nbrJoueruPartie >= 3 Then
            label3em.Visible = True
            pseudo3em.Visible = True
            label23em.Visible = True
            Score3em.Visible = True

            'Pseudo3em.Text = GlobalVariables.
            'Score3em.Text = GlobalVariables.

        Else
            label3em.Visible = False
            pseudo3em.Visible = False
            label23em.Visible = False
            Score3em.Visible = False
        End If

        If nbrJoueruPartie >= 4 Then
            label4em.Visible = True
            Pseudo4em.Visible = True
            label24em.Visible = True
            Score4em.Visible = True

            'Pseudo4em.Text = GlobalVariables.
            'Score4em.Text = GlobalVariables.

        Else
            label4em.Visible = False
            Pseudo4em.Visible = False
            label24em.Visible = False
            Score4em.Visible = False
        End If

    End Sub



    'Btn Quitter
    Private Sub BtnQuitter_Click(sender As Object, e As EventArgs) Handles BtnQuitter.Click
        Quitter_timer.Enabled = True
        BtnQuitter.BackgroundImage = My.Resources.c_btn_quitter_Enfoncer
    End Sub
    Private Sub BtnQuitter_MouseHover(sender As Object, e As EventArgs) Handles BtnQuitter.MouseHover
        BtnQuitter.BackgroundImage = My.Resources.b_btn_quitter_sourisAuDessu
    End Sub
    Private Sub BtnQuitter_MouseLeave(sender As Object, e As EventArgs) Handles BtnQuitter.MouseLeave
        BtnQuitter.BackgroundImage = My.Resources.a_btn_quitter_nonEnfoncer
    End Sub

    'Btn retour menu
    Private Sub MenuBtn_Click(sender As Object, e As EventArgs) Handles MenuBtn.Click
        Menu_timer.Enabled = True

    End Sub
    Private Sub MenuBtn_MouseHover(sender As Object, e As EventArgs) Handles MenuBtn.MouseHover

    End Sub
    Private Sub MenuBtn_MouseLeave(sender As Object, e As EventArgs) Handles MenuBtn.MouseLeave

    End Sub



    'Timer btn
    Private Sub Menu_timer_Tick(sender As Object, e As EventArgs) Handles Menu_timer.Tick
        Menu_timer.Enabled = False

        'Ouverture du menu
        Dim menumenu As MenuduJeu = New MenuduJeu
        menumenu.Width = GlobalVariables.largeur
        menumenu.Height = GlobalVariables.hauteur

        menumenu.Show()

        'fermeture de l'affichage des resultats
        Me.Close()

    End Sub
    Private Sub Quitter_timer_Tick(sender As Object, e As EventArgs) Handles Quitter_timer.Tick
        BtnQuitter.BackgroundImage = My.Resources.b_btn_quitter_sourisAuDessu
        Quitter_timer.Enabled = False

        GlobalVariables.song.ClickSong(2, False)

        Me.Owner = PlateauDeJeu
        PlateauDeJeu.Close()
        Me.Close()
    End Sub
End Class