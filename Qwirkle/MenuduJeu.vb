﻿'Imports System.ComponentModel
'Imports Qwirkle

Public Class MenuduJeu

    Private Sub Menu_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If GlobalVariables.song.getMute() = True Then
            song_o_f.BackgroundImage = My.Resources.sicOff
        Else
            song_o_f.BackgroundImage = My.Resources.sicOn
        End If
    End Sub

    'Gestion comportement bouton jouer 
    Private Sub BtnJouer_Click(sender As Object, e As EventArgs) Handles BtnJouer.Click
        BtnJouer.BackgroundImage = My.Resources.c_btn_jouer_Enfoncer
        timer_Jouer.Enabled = True
    End Sub
    Private Sub BtnJouer_MouseHover(sender As Object, e As EventArgs) Handles BtnJouer.MouseHover
        BtnJouer.BackgroundImage = My.Resources.b_btn_jouer_sourisAuDessu
    End Sub
    Private Sub BtnJouer_MouseLeave(sender As Object, e As EventArgs) Handles BtnJouer.MouseLeave
        BtnJouer.BackgroundImage = My.Resources.a_btn_jouer_nonEnfoncer
    End Sub

    'Gestion bouton Régles
    Private Sub BtnRegles_Click(sender As Object, e As EventArgs) Handles BtnRegles.Click
        BtnRegles.BackgroundImage = My.Resources.c_btn_regles_Enfoncer
        timer_Regles.Enabled = True
    End Sub
    Private Sub BtnRegles_MouseHover(sender As Object, e As EventArgs) Handles BtnRegles.MouseHover
        BtnRegles.BackgroundImage = My.Resources.b_btn_regles_sourisAuDessu
    End Sub
    Private Sub BtnRegles_MouseLeave(sender As Object, e As EventArgs) Handles BtnRegles.MouseLeave
        BtnRegles.BackgroundImage = My.Resources.a_btn_regles_nonEnfoncer
    End Sub

    'Gestion bouton Quitter
    Private Sub BtnQuitter_Click(sender As Object, e As EventArgs) Handles BtnQuitter.Click
        BtnQuitter.BackgroundImage = My.Resources.c_btn_quitter_Enfoncer
        timer_Quitter.Enabled = True
    End Sub
    Private Sub BtnQuitter_MouseHover(sender As Object, e As EventArgs) Handles BtnQuitter.MouseHover
        BtnQuitter.BackgroundImage = My.Resources.b_btn_quitter_sourisAuDessu
    End Sub
    Private Sub BtnQuitter_MouseLeave(sender As Object, e As EventArgs) Handles BtnQuitter.MouseLeave
        BtnQuitter.BackgroundImage = My.Resources.a_btn_quitter_nonEnfoncer
    End Sub

    'Gestion bouton Credit
    Private Sub BtnCredit_Click(sender As Object, e As EventArgs) Handles BtnCredit.Click
        BtnCredit.BackgroundImage = My.Resources.c_btn_credit_Enfoncer
        timer_Credit.Enabled = True
    End Sub
    Private Sub BtnCredit_MouseHover(sender As Object, e As EventArgs) Handles BtnCredit.MouseHover
        BtnCredit.BackgroundImage = My.Resources.b_btn_credit_sourisAuDessu
    End Sub
    Private Sub BtnCredit_MouseLeave(sender As Object, e As EventArgs) Handles BtnCredit.MouseLeave
        BtnCredit.BackgroundImage = My.Resources.a_btn_credit_nonEnfoncer
    End Sub

    'Les timers de mort
    Private Sub timer_Jouer_Tick(sender As Object, e As EventArgs) Handles timer_Jouer.Tick
        BtnJouer.BackgroundImage = My.Resources.b_btn_jouer_sourisAuDessu
        timer_Jouer.Enabled = False

        GlobalVariables.song.ClickSong(1, False)

        'Ouverture Parametre du jeu
        Dim Form As New ParametreJouer
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, Form, Me)
    End Sub
    Private Sub timer_Regles_Tick(sender As Object, e As EventArgs) Handles timer_Regles.Tick
        BtnRegles.BackgroundImage = My.Resources.b_btn_regles_sourisAuDessu
        timer_Regles.Enabled = False

        GlobalVariables.song.ClickSong(1, False)

        'Ouverture des regles 
        Dim Formregles As New Regles
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, Formregles, Me)
    End Sub
    Private Sub timer_Quitter_Tick(sender As Object, e As EventArgs) Handles timer_Quitter.Tick
        BtnQuitter.BackgroundImage = My.Resources.b_btn_quitter_sourisAuDessu
        timer_Quitter.Enabled = False

        GlobalVariables.song.ClickSong(2, False)

        'On quitte le programme
        Owner.Close()
    End Sub
    Private Sub timer_Credit_Tick(sender As Object, e As EventArgs) Handles timer_Credit.Tick
        BtnCredit.BackgroundImage = My.Resources.b_btn_credit_sourisAuDessu
        timer_Credit.Enabled = False

        GlobalVariables.song.ClickSong(1, False)

        'On ouvre les credits
        Dim Formcredit As New Credit
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, Formcredit, Me)
    End Sub

    Private Sub song_o_f_Click(sender As Object, e As EventArgs) Handles song_o_f.Click
        Dim estMute As Boolean = GlobalVariables.song.getMute()

        If estMute = False Then
            GlobalVariables.song.onMuteLeSon(True)
            song_o_f.BackgroundImage = My.Resources.sicOff
        Else
            GlobalVariables.song.onMuteLeSon(False)
            song_o_f.BackgroundImage = My.Resources.sicOn
        End If
    End Sub

    'easter egg
    Private Sub Logo_Click(sender As Object, e As EventArgs) Handles Logo.Click
        'Bientot ;)
    End Sub
End Class
