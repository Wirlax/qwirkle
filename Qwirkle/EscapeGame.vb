﻿Public Class EscapeGame
    'Chargement du form
    Private Sub EscapeGame_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If GlobalVariables.song.getMute() = True Then
            btnMusique.BackgroundImage = My.Resources.sicOff
        Else
            btnMusique.BackgroundImage = My.Resources.sicOn
        End If
    End Sub

    'Bouton pour la musique
    Private Sub btnMusique_Click(sender As Object, e As EventArgs) Handles btnMusique.Click
        Dim estMute As Boolean = GlobalVariables.song.getMute()
        If estMute = False Then
            GlobalVariables.song.onMuteLeSon(True)
            btnMusique.BackgroundImage = My.Resources.sicOff
        Else
            GlobalVariables.song.onMuteLeSon(False)
            btnMusique.BackgroundImage = My.Resources.sicOn
        End If
    End Sub


    'Bouton resume
    Private Sub btnResume_Click(sender As Object, e As EventArgs) Handles btnResume.Click
        Resume_timer.Enabled = True
        btnResume.BackgroundImage = My.Resources.c_btn_resume_Enfoncer
    End Sub
    Private Sub btnResume_MouseHover(sender As Object, e As EventArgs) Handles btnResume.MouseHover
        btnResume.BackgroundImage = My.Resources.b_btn_resume_sourisAuDessu
    End Sub
    Private Sub btnResume_MouseLeave(sender As Object, e As EventArgs) Handles btnResume.MouseLeave
        btnResume.BackgroundImage = My.Resources.a_btn_resume_nonEnfoncer
    End Sub

    'bouton quitter
    Private Sub btnQuitter_Click(sender As Object, e As EventArgs) Handles btnQuitter.Click
        Quitter_timer.Enabled = True
        btnQuitter.BackgroundImage = My.Resources.c_btn_quitter_Enfoncer
    End Sub
    Private Sub btnQuitter_MouseHover(sender As Object, e As EventArgs) Handles btnQuitter.MouseHover
        btnQuitter.BackgroundImage = My.Resources.b_btn_quitter_sourisAuDessu
    End Sub
    Private Sub btnQuitter_MouseLeave(sender As Object, e As EventArgs) Handles btnQuitter.MouseLeave
        btnQuitter.BackgroundImage = My.Resources.a_btn_quitter_nonEnfoncer
    End Sub


    'les timers
    Private Sub Resume_timer_Tick(sender As Object, e As EventArgs) Handles Resume_timer.Tick
        'on reprend le jeu
        Resume_timer.Enabled = False
        btnResume.BackgroundImage = My.Resources.b_btn_resume_sourisAuDessu
        GlobalVariables.song.ClickSong(1, False)

        Me.Close()
    End Sub
    Private Sub Quitter_timer_Tick(sender As Object, e As EventArgs) Handles Quitter_timer.Tick
        'On quitte le jeu
        btnQuitter.BackgroundImage = My.Resources.b_btn_quitter_sourisAuDessu
        Quitter_timer.Enabled = False

        GlobalVariables.song.ClickSong(2, False)

        Me.Owner = PlateauDeJeu
        PlateauDeJeu.Close()
        Me.Close()
    End Sub
End Class