﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PlateauDeJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PlateauDeJeu))
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnSwap = New System.Windows.Forms.PictureBox()
        Me.BtnFinTour = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Pioche6 = New System.Windows.Forms.PictureBox()
        Me.Piece_3_1_2 = New System.Windows.Forms.PictureBox()
        Me.Piece_4_1 = New System.Windows.Forms.PictureBox()
        Me.Piece_3_1_1 = New System.Windows.Forms.PictureBox()
        Me.Piece_3_4 = New System.Windows.Forms.PictureBox()
        Me.Piece_4_4 = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.Scorej4 = New System.Windows.Forms.Label()
        Me.Scorej3 = New System.Windows.Forms.Label()
        Me.Scorej2 = New System.Windows.Forms.Label()
        Me.Scorej1 = New System.Windows.Forms.Label()
        Me.LabelPseudoJ1 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LabelPseudoJ2 = New System.Windows.Forms.Label()
        Me.LabelPseudoJ3 = New System.Windows.Forms.Label()
        Me.LabelPseudoJ4 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.NbrTuilejoueurActu = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Swap_timer = New System.Windows.Forms.Timer(Me.components)
        Me.FinTour_timer = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.BtnSwap, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnFinTour, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        CType(Me.Pioche6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Piece_3_1_2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Piece_4_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Piece_3_1_1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Piece_3_4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Piece_4_4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.77273!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.22727!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel5, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel6, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 78.95652!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.04348!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1604, 886)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.79732!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.20268!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel4, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(242, 704)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1357, 176)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.BtnSwap, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.BtnFinTour, 0, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(907, 5)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(15, 5, 4, 5)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(446, 166)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'BtnSwap
        '
        Me.BtnSwap.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.BtnSwap.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_swap_nonEnfoncer
        Me.BtnSwap.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnSwap.Location = New System.Drawing.Point(82, 88)
        Me.BtnSwap.Margin = New System.Windows.Forms.Padding(4, 5, 60, 5)
        Me.BtnSwap.MaximumSize = New System.Drawing.Size(240, 123)
        Me.BtnSwap.MinimumSize = New System.Drawing.Size(225, 123)
        Me.BtnSwap.Name = "BtnSwap"
        Me.BtnSwap.Size = New System.Drawing.Size(225, 123)
        Me.BtnSwap.TabIndex = 1
        Me.BtnSwap.TabStop = False
        '
        'BtnFinTour
        '
        Me.BtnFinTour.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.BtnFinTour.BackgroundImage = Global.Qwirkle.My.Resources.Resources.btn_FinTour_nonEnfoncer
        Me.BtnFinTour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnFinTour.Location = New System.Drawing.Point(82, 5)
        Me.BtnFinTour.Margin = New System.Windows.Forms.Padding(4, 5, 60, 5)
        Me.BtnFinTour.MaximumSize = New System.Drawing.Size(240, 123)
        Me.BtnFinTour.MinimumSize = New System.Drawing.Size(225, 123)
        Me.BtnFinTour.Name = "BtnFinTour"
        Me.BtnFinTour.Size = New System.Drawing.Size(225, 123)
        Me.BtnFinTour.TabIndex = 0
        Me.BtnFinTour.TabStop = False
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 6
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel4.Controls.Add(Me.Pioche6, 5, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Piece_3_1_2, 4, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Piece_4_1, 2, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Piece_3_1_1, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Piece_3_4, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Piece_4_4, 3, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(30, 31)
        Me.TableLayoutPanel4.Margin = New System.Windows.Forms.Padding(30, 31, 30, 31)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 114.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(832, 114)
        Me.TableLayoutPanel4.TabIndex = 1
        '
        'Pioche6
        '
        Me.Pioche6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Pioche6.BackgroundImage = CType(resources.GetObject("Pioche6.BackgroundImage"), System.Drawing.Image)
        Me.Pioche6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Pioche6.Location = New System.Drawing.Point(720, 31)
        Me.Pioche6.Margin = New System.Windows.Forms.Padding(30, 31, 30, 31)
        Me.Pioche6.MaximumSize = New System.Drawing.Size(150, 154)
        Me.Pioche6.MinimumSize = New System.Drawing.Size(75, 77)
        Me.Pioche6.Name = "Pioche6"
        Me.Pioche6.Size = New System.Drawing.Size(82, 77)
        Me.Pioche6.TabIndex = 5
        Me.Pioche6.TabStop = False
        '
        'Piece_3_1_2
        '
        Me.Piece_3_1_2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Piece_3_1_2.BackgroundImage = Global.Qwirkle.My.Resources.Resources.piece_3_1
        Me.Piece_3_1_2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Piece_3_1_2.Location = New System.Drawing.Point(582, 31)
        Me.Piece_3_1_2.Margin = New System.Windows.Forms.Padding(30, 31, 30, 31)
        Me.Piece_3_1_2.MaximumSize = New System.Drawing.Size(150, 154)
        Me.Piece_3_1_2.MinimumSize = New System.Drawing.Size(75, 77)
        Me.Piece_3_1_2.Name = "Piece_3_1_2"
        Me.Piece_3_1_2.Size = New System.Drawing.Size(78, 77)
        Me.Piece_3_1_2.TabIndex = 4
        Me.Piece_3_1_2.TabStop = False
        '
        'Piece_4_1
        '
        Me.Piece_4_1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Piece_4_1.BackgroundImage = Global.Qwirkle.My.Resources.Resources.piece_4_1
        Me.Piece_4_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Piece_4_1.Location = New System.Drawing.Point(306, 31)
        Me.Piece_4_1.Margin = New System.Windows.Forms.Padding(30, 31, 30, 31)
        Me.Piece_4_1.MaximumSize = New System.Drawing.Size(150, 154)
        Me.Piece_4_1.MinimumSize = New System.Drawing.Size(75, 77)
        Me.Piece_4_1.Name = "Piece_4_1"
        Me.Piece_4_1.Size = New System.Drawing.Size(78, 77)
        Me.Piece_4_1.TabIndex = 2
        Me.Piece_4_1.TabStop = False
        '
        'Piece_3_1_1
        '
        Me.Piece_3_1_1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Piece_3_1_1.BackgroundImage = Global.Qwirkle.My.Resources.Resources.piece_3_1
        Me.Piece_3_1_1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Piece_3_1_1.Location = New System.Drawing.Point(168, 31)
        Me.Piece_3_1_1.Margin = New System.Windows.Forms.Padding(30, 31, 30, 31)
        Me.Piece_3_1_1.MaximumSize = New System.Drawing.Size(150, 154)
        Me.Piece_3_1_1.MinimumSize = New System.Drawing.Size(75, 77)
        Me.Piece_3_1_1.Name = "Piece_3_1_1"
        Me.Piece_3_1_1.Size = New System.Drawing.Size(78, 77)
        Me.Piece_3_1_1.TabIndex = 1
        Me.Piece_3_1_1.TabStop = False
        '
        'Piece_3_4
        '
        Me.Piece_3_4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Piece_3_4.BackgroundImage = Global.Qwirkle.My.Resources.Resources.piece_3_4
        Me.Piece_3_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Piece_3_4.Location = New System.Drawing.Point(30, 31)
        Me.Piece_3_4.Margin = New System.Windows.Forms.Padding(30, 31, 30, 31)
        Me.Piece_3_4.MaximumSize = New System.Drawing.Size(150, 154)
        Me.Piece_3_4.MinimumSize = New System.Drawing.Size(75, 77)
        Me.Piece_3_4.Name = "Piece_3_4"
        Me.Piece_3_4.Size = New System.Drawing.Size(78, 77)
        Me.Piece_3_4.TabIndex = 0
        Me.Piece_3_4.TabStop = False
        '
        'Piece_4_4
        '
        Me.Piece_4_4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Piece_4_4.BackgroundImage = Global.Qwirkle.My.Resources.Resources.piece_4_4
        Me.Piece_4_4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Piece_4_4.Location = New System.Drawing.Point(444, 31)
        Me.Piece_4_4.Margin = New System.Windows.Forms.Padding(30, 31, 30, 31)
        Me.Piece_4_4.MaximumSize = New System.Drawing.Size(150, 154)
        Me.Piece_4_4.MinimumSize = New System.Drawing.Size(75, 77)
        Me.Piece_4_4.Name = "Piece_4_4"
        Me.Piece_4_4.Size = New System.Drawing.Size(78, 77)
        Me.Piece_4_4.TabIndex = 3
        Me.Piece_4_4.TabStop = False
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.Scorej4, 0, 8)
        Me.TableLayoutPanel5.Controls.Add(Me.Scorej3, 0, 6)
        Me.TableLayoutPanel5.Controls.Add(Me.Scorej2, 0, 4)
        Me.TableLayoutPanel5.Controls.Add(Me.Scorej1, 0, 2)
        Me.TableLayoutPanel5.Controls.Add(Me.LabelPseudoJ1, 0, 1)
        Me.TableLayoutPanel5.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.LabelPseudoJ2, 0, 3)
        Me.TableLayoutPanel5.Controls.Add(Me.LabelPseudoJ3, 0, 5)
        Me.TableLayoutPanel5.Controls.Add(Me.LabelPseudoJ4, 0, 7)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(5, 6)
        Me.TableLayoutPanel5.Margin = New System.Windows.Forms.Padding(4, 5, 4, 23)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 9
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.76233!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.36024!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(228, 669)
        Me.TableLayoutPanel5.TabIndex = 1
        '
        'Scorej4
        '
        Me.Scorej4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Scorej4.AutoSize = True
        Me.Scorej4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Scorej4.ForeColor = System.Drawing.Color.Green
        Me.Scorej4.Location = New System.Drawing.Point(4, 612)
        Me.Scorej4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Scorej4.Name = "Scorej4"
        Me.Scorej4.Size = New System.Drawing.Size(220, 36)
        Me.Scorej4.TabIndex = 8
        Me.Scorej4.Text = "0"
        Me.Scorej4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Scorej3
        '
        Me.Scorej3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Scorej3.AutoSize = True
        Me.Scorej3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Scorej3.ForeColor = System.Drawing.Color.MediumBlue
        Me.Scorej3.Location = New System.Drawing.Point(4, 462)
        Me.Scorej3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Scorej3.Name = "Scorej3"
        Me.Scorej3.Size = New System.Drawing.Size(220, 36)
        Me.Scorej3.TabIndex = 7
        Me.Scorej3.Text = "0"
        Me.Scorej3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Scorej2
        '
        Me.Scorej2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Scorej2.AutoSize = True
        Me.Scorej2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Scorej2.ForeColor = System.Drawing.Color.Indigo
        Me.Scorej2.Location = New System.Drawing.Point(4, 315)
        Me.Scorej2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Scorej2.Name = "Scorej2"
        Me.Scorej2.Size = New System.Drawing.Size(220, 36)
        Me.Scorej2.TabIndex = 6
        Me.Scorej2.Text = "0"
        Me.Scorej2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Scorej1
        '
        Me.Scorej1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Scorej1.AutoSize = True
        Me.Scorej1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Scorej1.ForeColor = System.Drawing.Color.DarkRed
        Me.Scorej1.Location = New System.Drawing.Point(4, 167)
        Me.Scorej1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Scorej1.Name = "Scorej1"
        Me.Scorej1.Size = New System.Drawing.Size(220, 36)
        Me.Scorej1.TabIndex = 5
        Me.Scorej1.Text = "0"
        Me.Scorej1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelPseudoJ1
        '
        Me.LabelPseudoJ1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelPseudoJ1.AutoSize = True
        Me.LabelPseudoJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.LabelPseudoJ1.ForeColor = System.Drawing.Color.DarkRed
        Me.LabelPseudoJ1.Location = New System.Drawing.Point(4, 95)
        Me.LabelPseudoJ1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelPseudoJ1.MaximumSize = New System.Drawing.Size(332, 32)
        Me.LabelPseudoJ1.Name = "LabelPseudoJ1"
        Me.LabelPseudoJ1.Size = New System.Drawing.Size(220, 32)
        Me.LabelPseudoJ1.TabIndex = 1
        Me.LabelPseudoJ1.Text = "pseudo1 :"
        Me.LabelPseudoJ1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Goldenrod
        Me.Label1.Location = New System.Drawing.Point(4, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.MaximumSize = New System.Drawing.Size(332, 111)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(220, 74)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Score Joueurs :"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelPseudoJ2
        '
        Me.LabelPseudoJ2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelPseudoJ2.AutoSize = True
        Me.LabelPseudoJ2.BackColor = System.Drawing.Color.Transparent
        Me.LabelPseudoJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.LabelPseudoJ2.ForeColor = System.Drawing.Color.Indigo
        Me.LabelPseudoJ2.Location = New System.Drawing.Point(4, 243)
        Me.LabelPseudoJ2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelPseudoJ2.MaximumSize = New System.Drawing.Size(332, 32)
        Me.LabelPseudoJ2.Name = "LabelPseudoJ2"
        Me.LabelPseudoJ2.Size = New System.Drawing.Size(220, 32)
        Me.LabelPseudoJ2.TabIndex = 2
        Me.LabelPseudoJ2.Text = "pseudo2 :"
        Me.LabelPseudoJ2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelPseudoJ3
        '
        Me.LabelPseudoJ3.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelPseudoJ3.AutoSize = True
        Me.LabelPseudoJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.LabelPseudoJ3.ForeColor = System.Drawing.Color.MediumBlue
        Me.LabelPseudoJ3.Location = New System.Drawing.Point(4, 391)
        Me.LabelPseudoJ3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelPseudoJ3.MaximumSize = New System.Drawing.Size(332, 32)
        Me.LabelPseudoJ3.Name = "LabelPseudoJ3"
        Me.LabelPseudoJ3.Size = New System.Drawing.Size(220, 32)
        Me.LabelPseudoJ3.TabIndex = 3
        Me.LabelPseudoJ3.Text = "pseudo3 :"
        Me.LabelPseudoJ3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LabelPseudoJ4
        '
        Me.LabelPseudoJ4.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelPseudoJ4.AutoSize = True
        Me.LabelPseudoJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!)
        Me.LabelPseudoJ4.ForeColor = System.Drawing.Color.Green
        Me.LabelPseudoJ4.Location = New System.Drawing.Point(4, 538)
        Me.LabelPseudoJ4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LabelPseudoJ4.MaximumSize = New System.Drawing.Size(332, 32)
        Me.LabelPseudoJ4.Name = "LabelPseudoJ4"
        Me.LabelPseudoJ4.Size = New System.Drawing.Size(220, 32)
        Me.LabelPseudoJ4.TabIndex = 4
        Me.LabelPseudoJ4.Text = "pseudo4 :"
        Me.LabelPseudoJ4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 1
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.NbrTuilejoueurActu, 0, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.Label5, 0, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(5, 704)
        Me.TableLayoutPanel6.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 2
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 54.78261!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.21739!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(228, 176)
        Me.TableLayoutPanel6.TabIndex = 2
        '
        'NbrTuilejoueurActu
        '
        Me.NbrTuilejoueurActu.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NbrTuilejoueurActu.AutoSize = True
        Me.NbrTuilejoueurActu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NbrTuilejoueurActu.Location = New System.Drawing.Point(4, 118)
        Me.NbrTuilejoueurActu.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.NbrTuilejoueurActu.Name = "NbrTuilejoueurActu"
        Me.NbrTuilejoueurActu.Size = New System.Drawing.Size(220, 36)
        Me.NbrTuilejoueurActu.TabIndex = 4
        Me.NbrTuilejoueurActu.Text = "0"
        Me.NbrTuilejoueurActu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(4, 31)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.MaximumSize = New System.Drawing.Size(332, 34)
        Me.Label5.MinimumSize = New System.Drawing.Size(225, 31)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(225, 34)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Nombre de Tuiles :"
        '
        'Swap_timer
        '
        '
        'FinTour_timer
        '
        '
        'PlateauDeJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1604, 886)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "PlateauDeJeu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PlateauDeJeu"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.BtnSwap, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnFinTour, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        CType(Me.Pioche6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Piece_3_1_2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Piece_4_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Piece_3_1_1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Piece_3_4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Piece_4_4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel5 As TableLayoutPanel
    Friend WithEvents BtnSwap As PictureBox
    Friend WithEvents BtnFinTour As PictureBox
    Friend WithEvents Pioche6 As PictureBox
    Friend WithEvents Piece_3_1_2 As PictureBox
    Friend WithEvents Piece_4_4 As PictureBox
    Friend WithEvents Piece_4_1 As PictureBox
    Friend WithEvents Piece_3_1_1 As PictureBox
    Friend WithEvents Scorej4 As Label
    Friend WithEvents Scorej3 As Label
    Friend WithEvents Scorej2 As Label
    Friend WithEvents Scorej1 As Label
    Friend WithEvents LabelPseudoJ1 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents LabelPseudoJ2 As Label
    Friend WithEvents LabelPseudoJ3 As Label
    Friend WithEvents LabelPseudoJ4 As Label
    Friend WithEvents TableLayoutPanel6 As TableLayoutPanel
    Friend WithEvents NbrTuilejoueurActu As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Swap_timer As Timer
    Friend WithEvents FinTour_timer As Timer
    Friend WithEvents Piece_3_4 As PictureBox
End Class
