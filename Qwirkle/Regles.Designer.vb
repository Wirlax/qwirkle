﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Regles
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.timer_Retour = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.BtnRetour = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnDebPartie = New System.Windows.Forms.PictureBox()
        Me.BtnPendantTour = New System.Windows.Forms.PictureBox()
        Me.BtnCalcScore = New System.Windows.Forms.PictureBox()
        Me.BtnFinPartie = New System.Windows.Forms.PictureBox()
        Me.AffichageRegles = New System.Windows.Forms.PictureBox()
        Me.GLHF = New System.Windows.Forms.PictureBox()
        Me.LesRegles = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnRetour, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.BtnDebPartie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnPendantTour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnCalcScore, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnFinPartie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AffichageRegles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GLHF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LesRegles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'timer_Retour
        '
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.48737!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.51263!))
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox14, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BtnRetour, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.AffichageRegles, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.GLHF, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LesRegles, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.5174!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.08121!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.5174!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(2376, 1326)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'PictureBox14
        '
        Me.PictureBox14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox14.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox14.BackgroundImage = Global.Qwirkle.My.Resources.Resources.LOGO
        Me.PictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox14.Location = New System.Drawing.Point(1971, 23)
        Me.PictureBox14.Margin = New System.Windows.Forms.Padding(4, 23, 30, 5)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(375, 115)
        Me.PictureBox14.TabIndex = 13
        Me.PictureBox14.TabStop = False
        '
        'BtnRetour
        '
        Me.BtnRetour.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnRetour.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_retour_nonEnfoncer
        Me.BtnRetour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnRetour.Location = New System.Drawing.Point(30, 1172)
        Me.BtnRetour.Margin = New System.Windows.Forms.Padding(30, 5, 4, 31)
        Me.BtnRetour.Name = "BtnRetour"
        Me.BtnRetour.Size = New System.Drawing.Size(225, 123)
        Me.BtnRetour.TabIndex = 0
        Me.BtnRetour.TabStop = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.BtnDebPartie, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.BtnPendantTour, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.BtnCalcScore, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.BtnFinPartie, 0, 3)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(4, 237)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 4
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1001, 851)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'BtnDebPartie
        '
        Me.BtnDebPartie.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnDebPartie.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Deb_Partie
        Me.BtnDebPartie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnDebPartie.Location = New System.Drawing.Point(188, 10)
        Me.BtnDebPartie.Margin = New System.Windows.Forms.Padding(188, 5, 4, 5)
        Me.BtnDebPartie.Name = "BtnDebPartie"
        Me.BtnDebPartie.Size = New System.Drawing.Size(375, 192)
        Me.BtnDebPartie.TabIndex = 0
        Me.BtnDebPartie.TabStop = False
        '
        'BtnPendantTour
        '
        Me.BtnPendantTour.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnPendantTour.BackgroundImage = Global.Qwirkle.My.Resources.Resources.pendantTour
        Me.BtnPendantTour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnPendantTour.Location = New System.Drawing.Point(188, 222)
        Me.BtnPendantTour.Margin = New System.Windows.Forms.Padding(188, 5, 4, 5)
        Me.BtnPendantTour.Name = "BtnPendantTour"
        Me.BtnPendantTour.Size = New System.Drawing.Size(375, 192)
        Me.BtnPendantTour.TabIndex = 1
        Me.BtnPendantTour.TabStop = False
        '
        'BtnCalcScore
        '
        Me.BtnCalcScore.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnCalcScore.BackgroundImage = Global.Qwirkle.My.Resources.Resources.calcScore
        Me.BtnCalcScore.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnCalcScore.Location = New System.Drawing.Point(188, 434)
        Me.BtnCalcScore.Margin = New System.Windows.Forms.Padding(188, 5, 4, 5)
        Me.BtnCalcScore.Name = "BtnCalcScore"
        Me.BtnCalcScore.Size = New System.Drawing.Size(375, 192)
        Me.BtnCalcScore.TabIndex = 2
        Me.BtnCalcScore.TabStop = False
        '
        'BtnFinPartie
        '
        Me.BtnFinPartie.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.BtnFinPartie.BackgroundImage = Global.Qwirkle.My.Resources.Resources.finpartie
        Me.BtnFinPartie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnFinPartie.Location = New System.Drawing.Point(188, 647)
        Me.BtnFinPartie.Margin = New System.Windows.Forms.Padding(188, 5, 4, 5)
        Me.BtnFinPartie.Name = "BtnFinPartie"
        Me.BtnFinPartie.Size = New System.Drawing.Size(375, 192)
        Me.BtnFinPartie.TabIndex = 3
        Me.BtnFinPartie.TabStop = False
        '
        'AffichageRegles
        '
        Me.AffichageRegles.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.AffichageRegles.BackColor = System.Drawing.Color.Transparent
        Me.AffichageRegles.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Deb_Partie_EXPLICATION
        Me.AffichageRegles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AffichageRegles.Location = New System.Drawing.Point(1031, 278)
        Me.AffichageRegles.Margin = New System.Windows.Forms.Padding(22, 46, 30, 46)
        Me.AffichageRegles.Name = "AffichageRegles"
        Me.AffichageRegles.Size = New System.Drawing.Size(1315, 769)
        Me.AffichageRegles.TabIndex = 2
        Me.AffichageRegles.TabStop = False
        '
        'GLHF
        '
        Me.GLHF.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GLHF.BackColor = System.Drawing.Color.Transparent
        Me.GLHF.BackgroundImage = Global.Qwirkle.My.Resources.Resources.GLHF
        Me.GLHF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.GLHF.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.GLHF.Location = New System.Drawing.Point(1520, 1108)
        Me.GLHF.Margin = New System.Windows.Forms.Padding(4, 15, 4, 5)
        Me.GLHF.Name = "GLHF"
        Me.GLHF.Size = New System.Drawing.Size(345, 177)
        Me.GLHF.TabIndex = 3
        Me.GLHF.TabStop = False
        '
        'LesRegles
        '
        Me.LesRegles.BackColor = System.Drawing.Color.Transparent
        Me.LesRegles.BackgroundImage = Global.Qwirkle.My.Resources.Resources.LesRegles
        Me.LesRegles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.LesRegles.Location = New System.Drawing.Point(90, 54)
        Me.LesRegles.Margin = New System.Windows.Forms.Padding(90, 54, 4, 15)
        Me.LesRegles.Name = "LesRegles"
        Me.LesRegles.Size = New System.Drawing.Size(525, 162)
        Me.LesRegles.TabIndex = 4
        Me.LesRegles.TabStop = False
        '
        'Regles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(2376, 1326)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Regles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Regles"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnRetour, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.BtnDebPartie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnPendantTour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnCalcScore, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnFinPartie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AffichageRegles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GLHF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LesRegles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents BtnRetour As PictureBox
    Friend WithEvents timer_Retour As Timer
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents BtnDebPartie As PictureBox
    Friend WithEvents BtnPendantTour As PictureBox
    Friend WithEvents BtnCalcScore As PictureBox
    Friend WithEvents BtnFinPartie As PictureBox
    Friend WithEvents AffichageRegles As PictureBox
    Friend WithEvents GLHF As PictureBox
    Friend WithEvents LesRegles As PictureBox
    Friend WithEvents PictureBox14 As PictureBox
End Class
