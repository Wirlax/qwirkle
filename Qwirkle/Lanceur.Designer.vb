﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Lanceur
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Resolution = New System.Windows.Forms.ComboBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Fenetre = New System.Windows.Forms.RadioButton()
        Me.PleinEcran = New System.Windows.Forms.RadioButton()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(197, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 101)
        Me.Panel1.TabIndex = 0
        '
        'Resolution
        '
        Me.Resolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Resolution.FormattingEnabled = True
        Me.Resolution.Items.AddRange(New Object() {"1920 x 1080", "1600 x 900", "1366 x 768", "1024 x 768", "800 x 600", "Taille de l'écran"})
        Me.Resolution.Location = New System.Drawing.Point(43, 35)
        Me.Resolution.Name = "Resolution"
        Me.Resolution.Size = New System.Drawing.Size(121, 21)
        Me.Resolution.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.Resolution)
        Me.Panel2.Location = New System.Drawing.Point(0, 250)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(180, 100)
        Me.Panel2.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Résolution"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Button1)
        Me.Panel3.Controls.Add(Me.Fenetre)
        Me.Panel3.Controls.Add(Me.PleinEcran)
        Me.Panel3.Location = New System.Drawing.Point(200, 250)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(180, 100)
        Me.Panel3.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(105, 77)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Valider"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Fenetre
        '
        Me.Fenetre.AutoSize = True
        Me.Fenetre.Location = New System.Drawing.Point(29, 40)
        Me.Fenetre.Name = "Fenetre"
        Me.Fenetre.Size = New System.Drawing.Size(64, 17)
        Me.Fenetre.TabIndex = 1
        Me.Fenetre.TabStop = True
        Me.Fenetre.Text = "Fenetré "
        Me.Fenetre.UseVisualStyleBackColor = True
        '
        'PleinEcran
        '
        Me.PleinEcran.AutoSize = True
        Me.PleinEcran.Location = New System.Drawing.Point(29, 17)
        Me.PleinEcran.Name = "PleinEcran"
        Me.PleinEcran.Size = New System.Drawing.Size(78, 17)
        Me.PleinEcran.TabIndex = 0
        Me.PleinEcran.TabStop = True
        Me.PleinEcran.Text = "Plein écran"
        Me.PleinEcran.UseVisualStyleBackColor = True
        '
        'Logo
        '
        Me.Logo.BackgroundImage = Global.Qwirkle.My.Resources.Resources.LOGO
        Me.Logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Logo.Location = New System.Drawing.Point(32, 54)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(321, 121)
        Me.Logo.TabIndex = 4
        Me.Logo.TabStop = False
        '
        'Lanceur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(384, 362)
        Me.Controls.Add(Me.Logo)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Lanceur"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lanceur"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Resolution As ComboBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Fenetre As RadioButton
    Friend WithEvents PleinEcran As RadioButton
    Friend WithEvents Logo As PictureBox
    Friend WithEvents Button1 As Button
End Class
