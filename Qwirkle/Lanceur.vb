﻿Public Class Lanceur
    Dim BtnFenetre As Boolean
    Dim BtnResolution As Boolean
    Dim hauteurSysteme As Int16
    Dim largeurSysteme As Int16

    'Vérification pour savoir si un mode d'affichage à été séléectionné avant de pouvoir valider 
    Private Sub Verification()
        If GlobalVariables.plein_ecran = True Or BtnFenetre = True And BtnResolution = True Then
            Button1.Enabled = True
        Else
            Button1.Enabled = False
        End If
    End Sub

    'Si le bouton plein écran est choisit les autres ne peuvent pas être cochés
    Private Sub PleinEcran_Click(sender As Object, e As EventArgs) Handles PleinEcran.Click
        GlobalVariables.plein_ecran = True
        BtnFenetre = False
        Resolution.Enabled = False
        Verification()
    End Sub
    'Même cas que au dessus mais avec le mode fenêtré
    Private Sub Fenetre_Click(sender As Object, e As EventArgs) Handles Fenetre.Click
        GlobalVariables.plein_ecran = False
        BtnFenetre = True
        Resolution.Enabled = True
        Verification()
    End Sub

    'Bouton valider and appel de ShowForm pour afficher le menu
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim Form As New MenuduJeu
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, Form, Me)
    End Sub


    'Si l'écran à une résolution plus petite que celles disponibles  les résolution non compatible sont alors retirée 
    Private Sub Lanceur_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GlobalVariables.plein_ecran = False
        Me.BtnFenetre = False
        Me.BtnResolution = False
        hauteurSysteme = Screen.PrimaryScreen.WorkingArea.Size.Height
        largeurSysteme = Screen.PrimaryScreen.WorkingArea.Size.Width
        If hauteurSysteme < 1080 Or largeurSysteme < 1920 Then
            Resolution.Items.RemoveAt(0)
        ElseIf hauteurSysteme < 900 Or largeurSysteme < 1600 Then
            Resolution.Items.RemoveAt(1)
        ElseIf hauteurSysteme < 768 Or largeurSysteme < 1366 Then
            Resolution.Items.RemoveAt(2)
        ElseIf hauteurSysteme < 768 Or largeurSysteme < 1024 Then
            Resolution.Items.RemoveAt(3)
        End If
    End Sub

    'donne la résolution pour l'affichage en fonction du choix 
    Private Sub Resolution_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Resolution.SelectedIndexChanged
        BtnResolution = True
        Select Case Resolution.SelectedItem
            Case "1920 x 1080"
                GlobalVariables.hauteur = 1080
                GlobalVariables.largeur = 1920
            Case "1600 x 900"
                GlobalVariables.hauteur = 900
                GlobalVariables.largeur = 1600
            Case "1366 x 768"
                GlobalVariables.hauteur = 768
                GlobalVariables.largeur = 1366
            Case "1024 x 768"
                GlobalVariables.hauteur = 768
                GlobalVariables.largeur = 1024
            Case "800 x 600"
                GlobalVariables.hauteur = 600
                GlobalVariables.largeur = 800
            Case "Taille de l'écran"
                GlobalVariables.hauteur = hauteurSysteme
                GlobalVariables.largeur = largeurSysteme
        End Select


        Verification()

    End Sub
End Class