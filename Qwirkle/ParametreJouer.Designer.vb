﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ParametreJouer
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.timer_Retour = New System.Windows.Forms.Timer(Me.components)
        Me.timer_LetsGo = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.BtnRetour = New System.Windows.Forms.PictureBox()
        Me.BtnLetsGo = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.tempsPartie = New System.Windows.Forms.NumericUpDown()
        Me.txt_tempsPartie = New System.Windows.Forms.PictureBox()
        Me.nbrJoueur = New System.Windows.Forms.NumericUpDown()
        Me.txt_nbrJoueur = New System.Windows.Forms.PictureBox()
        Me.parametrepartie = New System.Windows.Forms.PictureBox()
        Me.PannelJoueur1 = New System.Windows.Forms.TableLayoutPanel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PseudoJoueur1 = New System.Windows.Forms.TextBox()
        Me.ddnj1 = New System.Windows.Forms.DateTimePicker()
        Me.PannelJoueur3 = New System.Windows.Forms.TableLayoutPanel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.txt_joueur3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PseudoJoueur3 = New System.Windows.Forms.TextBox()
        Me.ddnj3 = New System.Windows.Forms.DateTimePicker()
        Me.pannelJoueur4 = New System.Windows.Forms.TableLayoutPanel()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.txt_joueur4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PseudoJoueur4 = New System.Windows.Forms.TextBox()
        Me.ddnj4 = New System.Windows.Forms.DateTimePicker()
        Me.PannelJoueur2 = New System.Windows.Forms.TableLayoutPanel()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.PseudoJoueur2 = New System.Windows.Forms.TextBox()
        Me.ddnj2 = New System.Windows.Forms.DateTimePicker()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.BtnRetour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnLetsGo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.tempsPartie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_tempsPartie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nbrJoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_nbrJoueur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.parametrepartie, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PannelJoueur1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PannelJoueur3.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_joueur3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pannelJoueur4.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txt_joueur4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PannelJoueur2.SuspendLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'timer_Retour
        '
        '
        'timer_LetsGo
        '
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.BtnRetour, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.BtnLetsGo, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.parametrepartie, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PannelJoueur1, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PannelJoueur3, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.pannelJoueur4, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PannelJoueur2, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox14, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.22222!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.22222!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.8149!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.1535!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.46275!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(2400, 1329)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'BtnRetour
        '
        Me.BtnRetour.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnRetour.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_retour_nonEnfoncer
        Me.BtnRetour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnRetour.Location = New System.Drawing.Point(30, 1179)
        Me.BtnRetour.Margin = New System.Windows.Forms.Padding(30, 4, 4, 30)
        Me.BtnRetour.MaximumSize = New System.Drawing.Size(225, 120)
        Me.BtnRetour.MinimumSize = New System.Drawing.Size(150, 75)
        Me.BtnRetour.Name = "BtnRetour"
        Me.BtnRetour.Size = New System.Drawing.Size(225, 120)
        Me.BtnRetour.TabIndex = 1
        Me.BtnRetour.TabStop = False
        '
        'BtnLetsGo
        '
        Me.BtnLetsGo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnLetsGo.BackgroundImage = Global.Qwirkle.My.Resources.Resources.a_btn_letsgo_nonEnfoncer
        Me.BtnLetsGo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnLetsGo.Location = New System.Drawing.Point(2100, 1164)
        Me.BtnLetsGo.Margin = New System.Windows.Forms.Padding(4, 4, 30, 30)
        Me.BtnLetsGo.MaximumSize = New System.Drawing.Size(270, 135)
        Me.BtnLetsGo.MinimumSize = New System.Drawing.Size(150, 75)
        Me.BtnLetsGo.Name = "BtnLetsGo"
        Me.BtnLetsGo.Size = New System.Drawing.Size(270, 135)
        Me.BtnLetsGo.TabIndex = 3
        Me.BtnLetsGo.TabStop = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.07305!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.92695!))
        Me.TableLayoutPanel2.Controls.Add(Me.tempsPartie, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.txt_tempsPartie, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.nbrJoueur, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txt_nbrJoueur, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(4, 233)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1192, 247)
        Me.TableLayoutPanel2.TabIndex = 5
        '
        'tempsPartie
        '
        Me.tempsPartie.BackColor = System.Drawing.Color.Khaki
        Me.tempsPartie.CausesValidation = False
        Me.tempsPartie.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.tempsPartie.ForeColor = System.Drawing.Color.Goldenrod
        Me.tempsPartie.Location = New System.Drawing.Point(528, 97)
        Me.tempsPartie.Margin = New System.Windows.Forms.Padding(15, 15, 4, 4)
        Me.tempsPartie.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.tempsPartie.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.tempsPartie.Name = "tempsPartie"
        Me.tempsPartie.Size = New System.Drawing.Size(82, 53)
        Me.tempsPartie.TabIndex = 6
        Me.tempsPartie.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txt_tempsPartie
        '
        Me.txt_tempsPartie.BackgroundImage = Global.Qwirkle.My.Resources.Resources.tempspartie1
        Me.txt_tempsPartie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.txt_tempsPartie.Location = New System.Drawing.Point(120, 86)
        Me.txt_tempsPartie.Margin = New System.Windows.Forms.Padding(120, 4, 4, 4)
        Me.txt_tempsPartie.Name = "txt_tempsPartie"
        Me.txt_tempsPartie.Size = New System.Drawing.Size(387, 72)
        Me.txt_tempsPartie.TabIndex = 5
        Me.txt_tempsPartie.TabStop = False
        '
        'nbrJoueur
        '
        Me.nbrJoueur.BackColor = System.Drawing.Color.RosyBrown
        Me.nbrJoueur.CausesValidation = False
        Me.nbrJoueur.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!)
        Me.nbrJoueur.ForeColor = System.Drawing.Color.Red
        Me.nbrJoueur.Location = New System.Drawing.Point(528, 15)
        Me.nbrJoueur.Margin = New System.Windows.Forms.Padding(15, 15, 4, 4)
        Me.nbrJoueur.Maximum = New Decimal(New Integer() {4, 0, 0, 0})
        Me.nbrJoueur.Minimum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nbrJoueur.Name = "nbrJoueur"
        Me.nbrJoueur.Size = New System.Drawing.Size(82, 53)
        Me.nbrJoueur.TabIndex = 4
        Me.nbrJoueur.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        'txt_nbrJoueur
        '
        Me.txt_nbrJoueur.BackgroundImage = Global.Qwirkle.My.Resources.Resources.nbrJoueur1
        Me.txt_nbrJoueur.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.txt_nbrJoueur.Location = New System.Drawing.Point(128, 4)
        Me.txt_nbrJoueur.Margin = New System.Windows.Forms.Padding(128, 4, 4, 4)
        Me.txt_nbrJoueur.Name = "txt_nbrJoueur"
        Me.txt_nbrJoueur.Size = New System.Drawing.Size(375, 72)
        Me.txt_nbrJoueur.TabIndex = 3
        Me.txt_nbrJoueur.TabStop = False
        '
        'parametrepartie
        '
        Me.parametrepartie.BackColor = System.Drawing.Color.Transparent
        Me.parametrepartie.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Parametrepartie
        Me.parametrepartie.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.parametrepartie.Location = New System.Drawing.Point(75, 30)
        Me.parametrepartie.Margin = New System.Windows.Forms.Padding(75, 30, 4, 4)
        Me.parametrepartie.Name = "parametrepartie"
        Me.parametrepartie.Size = New System.Drawing.Size(712, 150)
        Me.parametrepartie.TabIndex = 6
        Me.parametrepartie.TabStop = False
        '
        'PannelJoueur1
        '
        Me.PannelJoueur1.BackColor = System.Drawing.Color.Transparent
        Me.PannelJoueur1.ColumnCount = 2
        Me.PannelJoueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.PannelJoueur1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.PannelJoueur1.Controls.Add(Me.PictureBox1, 0, 0)
        Me.PannelJoueur1.Controls.Add(Me.PictureBox2, 0, 1)
        Me.PannelJoueur1.Controls.Add(Me.PictureBox3, 0, 2)
        Me.PannelJoueur1.Controls.Add(Me.PseudoJoueur1, 1, 1)
        Me.PannelJoueur1.Controls.Add(Me.ddnj1, 1, 2)
        Me.PannelJoueur1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PannelJoueur1.Location = New System.Drawing.Point(4, 488)
        Me.PannelJoueur1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PannelJoueur1.Name = "PannelJoueur1"
        Me.PannelJoueur1.RowCount = 3
        Me.PannelJoueur1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.24229!))
        Me.PannelJoueur1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28.57143!))
        Me.PannelJoueur1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.50794!))
        Me.PannelJoueur1.Size = New System.Drawing.Size(1192, 308)
        Me.PannelJoueur1.TabIndex = 7
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Joueur1
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Location = New System.Drawing.Point(30, 4)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(30, 4, 4, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(375, 90)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox2.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Pseudo_Joueur1
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Location = New System.Drawing.Point(165, 119)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(165, 4, 4, 4)
        Me.PictureBox2.MaximumSize = New System.Drawing.Size(255, 74)
        Me.PictureBox2.MinimumSize = New System.Drawing.Size(150, 45)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(255, 64)
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox3.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ddn1
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox3.Location = New System.Drawing.Point(75, 214)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(75, 4, 4, 4)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(375, 75)
        Me.PictureBox3.TabIndex = 2
        Me.PictureBox3.TabStop = False
        '
        'PseudoJoueur1
        '
        Me.PseudoJoueur1.BackColor = System.Drawing.Color.RosyBrown
        Me.PseudoJoueur1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PseudoJoueur1.ForeColor = System.Drawing.Color.DarkRed
        Me.PseudoJoueur1.Location = New System.Drawing.Point(611, 123)
        Me.PseudoJoueur1.Margin = New System.Windows.Forms.Padding(15, 15, 4, 4)
        Me.PseudoJoueur1.MaxLength = 15
        Me.PseudoJoueur1.Name = "PseudoJoueur1"
        Me.PseudoJoueur1.Size = New System.Drawing.Size(386, 48)
        Me.PseudoJoueur1.TabIndex = 3
        Me.PseudoJoueur1.Text = "jean"
        '
        'ddnj1
        '
        Me.ddnj1.CalendarForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj1.CalendarMonthBackground = System.Drawing.Color.DarkViolet
        Me.ddnj1.CalendarTitleBackColor = System.Drawing.Color.DarkViolet
        Me.ddnj1.CalendarTitleForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj1.CalendarTrailingForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj1.CausesValidation = False
        Me.ddnj1.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.ddnj1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ddnj1.Location = New System.Drawing.Point(611, 240)
        Me.ddnj1.Margin = New System.Windows.Forms.Padding(15, 45, 4, 4)
        Me.ddnj1.MaxDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.ddnj1.MinDate = New Date(1950, 1, 1, 0, 0, 0, 0)
        Me.ddnj1.Name = "ddnj1"
        Me.ddnj1.Size = New System.Drawing.Size(254, 44)
        Me.ddnj1.TabIndex = 6
        Me.ddnj1.Value = New Date(2014, 1, 1, 0, 0, 0, 0)
        '
        'PannelJoueur3
        '
        Me.PannelJoueur3.BackColor = System.Drawing.Color.Transparent
        Me.PannelJoueur3.ColumnCount = 2
        Me.PannelJoueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.PannelJoueur3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.PannelJoueur3.Controls.Add(Me.PictureBox7, 0, 2)
        Me.PannelJoueur3.Controls.Add(Me.txt_joueur3, 0, 0)
        Me.PannelJoueur3.Controls.Add(Me.PictureBox6, 0, 1)
        Me.PannelJoueur3.Controls.Add(Me.PseudoJoueur3, 1, 1)
        Me.PannelJoueur3.Controls.Add(Me.ddnj3, 1, 2)
        Me.PannelJoueur3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PannelJoueur3.Location = New System.Drawing.Point(1204, 488)
        Me.PannelJoueur3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PannelJoueur3.Name = "PannelJoueur3"
        Me.PannelJoueur3.RowCount = 3
        Me.PannelJoueur3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.37118!))
        Me.PannelJoueur3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 28.57143!))
        Me.PannelJoueur3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.50794!))
        Me.PannelJoueur3.Size = New System.Drawing.Size(1192, 308)
        Me.PannelJoueur3.TabIndex = 9
        '
        'PictureBox7
        '
        Me.PictureBox7.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox7.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ddn3
        Me.PictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox7.Location = New System.Drawing.Point(75, 214)
        Me.PictureBox7.Margin = New System.Windows.Forms.Padding(75, 4, 4, 4)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(375, 75)
        Me.PictureBox7.TabIndex = 3
        Me.PictureBox7.TabStop = False
        '
        'txt_joueur3
        '
        Me.txt_joueur3.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Joueur3
        Me.txt_joueur3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.txt_joueur3.Location = New System.Drawing.Point(30, 4)
        Me.txt_joueur3.Margin = New System.Windows.Forms.Padding(30, 4, 4, 4)
        Me.txt_joueur3.Name = "txt_joueur3"
        Me.txt_joueur3.Size = New System.Drawing.Size(375, 90)
        Me.txt_joueur3.TabIndex = 1
        Me.txt_joueur3.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox6.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Pseudo_Joueur3
        Me.PictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox6.Location = New System.Drawing.Point(165, 118)
        Me.PictureBox6.Margin = New System.Windows.Forms.Padding(165, 4, 4, 4)
        Me.PictureBox6.MaximumSize = New System.Drawing.Size(255, 74)
        Me.PictureBox6.MinimumSize = New System.Drawing.Size(150, 45)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(255, 66)
        Me.PictureBox6.TabIndex = 2
        Me.PictureBox6.TabStop = False
        '
        'PseudoJoueur3
        '
        Me.PseudoJoueur3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PseudoJoueur3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PseudoJoueur3.ForeColor = System.Drawing.Color.Navy
        Me.PseudoJoueur3.Location = New System.Drawing.Point(611, 123)
        Me.PseudoJoueur3.Margin = New System.Windows.Forms.Padding(15, 15, 4, 4)
        Me.PseudoJoueur3.MaxLength = 15
        Me.PseudoJoueur3.Name = "PseudoJoueur3"
        Me.PseudoJoueur3.Size = New System.Drawing.Size(386, 48)
        Me.PseudoJoueur3.TabIndex = 4
        Me.PseudoJoueur3.Text = "jean"
        '
        'ddnj3
        '
        Me.ddnj3.CalendarForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj3.CalendarMonthBackground = System.Drawing.Color.DarkViolet
        Me.ddnj3.CalendarTitleBackColor = System.Drawing.Color.DarkViolet
        Me.ddnj3.CalendarTitleForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj3.CalendarTrailingForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj3.CausesValidation = False
        Me.ddnj3.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.ddnj3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ddnj3.Location = New System.Drawing.Point(611, 240)
        Me.ddnj3.Margin = New System.Windows.Forms.Padding(15, 45, 4, 4)
        Me.ddnj3.MaxDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.ddnj3.MinDate = New Date(1950, 1, 1, 0, 0, 0, 0)
        Me.ddnj3.Name = "ddnj3"
        Me.ddnj3.Size = New System.Drawing.Size(254, 44)
        Me.ddnj3.TabIndex = 6
        Me.ddnj3.Value = New Date(2014, 1, 1, 0, 0, 0, 0)
        '
        'pannelJoueur4
        '
        Me.pannelJoueur4.BackColor = System.Drawing.Color.Transparent
        Me.pannelJoueur4.ColumnCount = 2
        Me.pannelJoueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pannelJoueur4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pannelJoueur4.Controls.Add(Me.PictureBox9, 0, 2)
        Me.pannelJoueur4.Controls.Add(Me.txt_joueur4, 0, 0)
        Me.pannelJoueur4.Controls.Add(Me.PictureBox8, 0, 1)
        Me.pannelJoueur4.Controls.Add(Me.PseudoJoueur4, 1, 1)
        Me.pannelJoueur4.Controls.Add(Me.ddnj4, 1, 2)
        Me.pannelJoueur4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pannelJoueur4.Location = New System.Drawing.Point(1204, 804)
        Me.pannelJoueur4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.pannelJoueur4.Name = "pannelJoueur4"
        Me.pannelJoueur4.RowCount = 3
        Me.pannelJoueur4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.67442!))
        Me.pannelJoueur4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.90698!))
        Me.pannelJoueur4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.41861!))
        Me.pannelJoueur4.Size = New System.Drawing.Size(1192, 313)
        Me.pannelJoueur4.TabIndex = 10
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox9.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ddn4
        Me.PictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox9.Location = New System.Drawing.Point(75, 221)
        Me.PictureBox9.Margin = New System.Windows.Forms.Padding(75, 4, 4, 4)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(375, 74)
        Me.PictureBox9.TabIndex = 3
        Me.PictureBox9.TabStop = False
        '
        'txt_joueur4
        '
        Me.txt_joueur4.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Joueur4
        Me.txt_joueur4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.txt_joueur4.Location = New System.Drawing.Point(30, 10)
        Me.txt_joueur4.Margin = New System.Windows.Forms.Padding(30, 10, 4, 4)
        Me.txt_joueur4.Name = "txt_joueur4"
        Me.txt_joueur4.Size = New System.Drawing.Size(375, 102)
        Me.txt_joueur4.TabIndex = 1
        Me.txt_joueur4.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox8.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Pseudo_Joueur4
        Me.PictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox8.Location = New System.Drawing.Point(165, 123)
        Me.PictureBox8.Margin = New System.Windows.Forms.Padding(165, 4, 4, 4)
        Me.PictureBox8.MaximumSize = New System.Drawing.Size(255, 74)
        Me.PictureBox8.MinimumSize = New System.Drawing.Size(150, 45)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(255, 74)
        Me.PictureBox8.TabIndex = 2
        Me.PictureBox8.TabStop = False
        '
        'PseudoJoueur4
        '
        Me.PseudoJoueur4.BackColor = System.Drawing.Color.PaleGreen
        Me.PseudoJoueur4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PseudoJoueur4.ForeColor = System.Drawing.Color.Green
        Me.PseudoJoueur4.Location = New System.Drawing.Point(611, 147)
        Me.PseudoJoueur4.Margin = New System.Windows.Forms.Padding(15, 30, 4, 4)
        Me.PseudoJoueur4.MaxLength = 15
        Me.PseudoJoueur4.Name = "PseudoJoueur4"
        Me.PseudoJoueur4.Size = New System.Drawing.Size(386, 48)
        Me.PseudoJoueur4.TabIndex = 4
        Me.PseudoJoueur4.Text = "jean"
        '
        'ddnj4
        '
        Me.ddnj4.CalendarForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj4.CalendarMonthBackground = System.Drawing.Color.DarkViolet
        Me.ddnj4.CalendarTitleBackColor = System.Drawing.Color.DarkViolet
        Me.ddnj4.CalendarTitleForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj4.CalendarTrailingForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj4.CausesValidation = False
        Me.ddnj4.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.ddnj4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ddnj4.Location = New System.Drawing.Point(611, 234)
        Me.ddnj4.Margin = New System.Windows.Forms.Padding(15, 30, 4, 4)
        Me.ddnj4.MaxDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.ddnj4.MinDate = New Date(1950, 1, 1, 0, 0, 0, 0)
        Me.ddnj4.Name = "ddnj4"
        Me.ddnj4.Size = New System.Drawing.Size(254, 44)
        Me.ddnj4.TabIndex = 6
        Me.ddnj4.Value = New Date(2014, 1, 1, 0, 0, 0, 0)
        '
        'PannelJoueur2
        '
        Me.PannelJoueur2.BackColor = System.Drawing.Color.Transparent
        Me.PannelJoueur2.ColumnCount = 2
        Me.PannelJoueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.PannelJoueur2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.PannelJoueur2.Controls.Add(Me.PictureBox13, 0, 2)
        Me.PannelJoueur2.Controls.Add(Me.PictureBox11, 0, 0)
        Me.PannelJoueur2.Controls.Add(Me.PictureBox12, 0, 1)
        Me.PannelJoueur2.Controls.Add(Me.PseudoJoueur2, 1, 1)
        Me.PannelJoueur2.Controls.Add(Me.ddnj2, 1, 2)
        Me.PannelJoueur2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PannelJoueur2.Location = New System.Drawing.Point(4, 804)
        Me.PannelJoueur2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.PannelJoueur2.Name = "PannelJoueur2"
        Me.PannelJoueur2.RowCount = 3
        Me.PannelJoueur2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.13953!))
        Me.PannelJoueur2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.90698!))
        Me.PannelJoueur2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.41861!))
        Me.PannelJoueur2.Size = New System.Drawing.Size(1192, 313)
        Me.PannelJoueur2.TabIndex = 11
        '
        'PictureBox13
        '
        Me.PictureBox13.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ddn2
        Me.PictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox13.Location = New System.Drawing.Point(75, 208)
        Me.PictureBox13.Margin = New System.Windows.Forms.Padding(75, 4, 4, 4)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(375, 74)
        Me.PictureBox13.TabIndex = 3
        Me.PictureBox13.TabStop = False
        '
        'PictureBox11
        '
        Me.PictureBox11.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Joueur2
        Me.PictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox11.Location = New System.Drawing.Point(30, 10)
        Me.PictureBox11.Margin = New System.Windows.Forms.Padding(30, 10, 4, 4)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(375, 102)
        Me.PictureBox11.TabIndex = 1
        Me.PictureBox11.TabStop = False
        '
        'PictureBox12
        '
        Me.PictureBox12.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox12.BackgroundImage = Global.Qwirkle.My.Resources.Resources.Pseudo_Joueur2
        Me.PictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox12.Location = New System.Drawing.Point(165, 124)
        Me.PictureBox12.Margin = New System.Windows.Forms.Padding(165, 4, 4, 4)
        Me.PictureBox12.MaximumSize = New System.Drawing.Size(255, 74)
        Me.PictureBox12.MinimumSize = New System.Drawing.Size(150, 45)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(255, 74)
        Me.PictureBox12.TabIndex = 2
        Me.PictureBox12.TabStop = False
        '
        'PseudoJoueur2
        '
        Me.PseudoJoueur2.BackColor = System.Drawing.Color.Thistle
        Me.PseudoJoueur2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PseudoJoueur2.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.PseudoJoueur2.Location = New System.Drawing.Point(611, 148)
        Me.PseudoJoueur2.Margin = New System.Windows.Forms.Padding(15, 30, 4, 4)
        Me.PseudoJoueur2.MaxLength = 15
        Me.PseudoJoueur2.Name = "PseudoJoueur2"
        Me.PseudoJoueur2.Size = New System.Drawing.Size(386, 48)
        Me.PseudoJoueur2.TabIndex = 4
        Me.PseudoJoueur2.Text = "jean"
        '
        'ddnj2
        '
        Me.ddnj2.CalendarForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj2.CalendarMonthBackground = System.Drawing.Color.DarkViolet
        Me.ddnj2.CalendarTitleBackColor = System.Drawing.Color.DarkViolet
        Me.ddnj2.CalendarTitleForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj2.CalendarTrailingForeColor = System.Drawing.Color.DarkViolet
        Me.ddnj2.CausesValidation = False
        Me.ddnj2.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!)
        Me.ddnj2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.ddnj2.Location = New System.Drawing.Point(611, 234)
        Me.ddnj2.Margin = New System.Windows.Forms.Padding(15, 30, 4, 4)
        Me.ddnj2.MaxDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.ddnj2.MinDate = New Date(1950, 1, 1, 0, 0, 0, 0)
        Me.ddnj2.Name = "ddnj2"
        Me.ddnj2.Size = New System.Drawing.Size(254, 44)
        Me.ddnj2.TabIndex = 5
        Me.ddnj2.Value = New Date(2014, 1, 1, 0, 0, 0, 0)
        '
        'PictureBox14
        '
        Me.PictureBox14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox14.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox14.BackgroundImage = Global.Qwirkle.My.Resources.Resources.LOGO
        Me.PictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox14.Location = New System.Drawing.Point(1995, 22)
        Me.PictureBox14.Margin = New System.Windows.Forms.Padding(4, 22, 30, 4)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(375, 112)
        Me.PictureBox14.TabIndex = 12
        Me.PictureBox14.TabStop = False
        '
        'ParametreJouer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(144.0!, 144.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(2400, 1329)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "ParametreJouer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ParametreJouer"
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.BtnRetour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnLetsGo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.tempsPartie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_tempsPartie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nbrJoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_nbrJoueur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.parametrepartie, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PannelJoueur1.ResumeLayout(False)
        Me.PannelJoueur1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PannelJoueur3.ResumeLayout(False)
        Me.PannelJoueur3.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_joueur3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pannelJoueur4.ResumeLayout(False)
        Me.pannelJoueur4.PerformLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txt_joueur4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PannelJoueur2.ResumeLayout(False)
        Me.PannelJoueur2.PerformLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents BtnRetour As PictureBox
    Friend WithEvents timer_Retour As Timer
    Friend WithEvents BtnLetsGo As PictureBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents nbrJoueur As NumericUpDown
    Friend WithEvents txt_nbrJoueur As PictureBox
    Friend WithEvents parametrepartie As PictureBox
    Friend WithEvents tempsPartie As NumericUpDown
    Friend WithEvents txt_tempsPartie As PictureBox
    Friend WithEvents PannelJoueur1 As TableLayoutPanel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents PannelJoueur3 As TableLayoutPanel
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents txt_joueur3 As PictureBox
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents pannelJoueur4 As TableLayoutPanel
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents txt_joueur4 As PictureBox
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents TableLayoutPanel4 As TableLayoutPanel
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents PannelJoueur2 As TableLayoutPanel
    Friend WithEvents PictureBox13 As PictureBox
    Friend WithEvents PictureBox11 As PictureBox
    Friend WithEvents PictureBox12 As PictureBox
    Friend WithEvents PseudoJoueur1 As TextBox
    Friend WithEvents PseudoJoueur3 As TextBox
    Friend WithEvents PseudoJoueur4 As TextBox
    Friend WithEvents PseudoJoueur2 As TextBox
    Friend WithEvents timer_LetsGo As Timer
    Friend WithEvents ddnj2 As DateTimePicker
    Friend WithEvents ddnj1 As DateTimePicker
    Friend WithEvents ddnj3 As DateTimePicker
    Friend WithEvents ddnj4 As DateTimePicker
    Friend WithEvents PictureBox14 As PictureBox
End Class
