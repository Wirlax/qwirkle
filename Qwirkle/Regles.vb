﻿Public Class Regles

    'Private Sub Regles_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    'End Sub

    'Gestion bouton Retour
    Private Sub BtnRetour_Click(sender As Object, e As EventArgs) Handles BtnRetour.Click
        BtnRetour.BackgroundImage = My.Resources.c_btn_retour_Enfoncer
        timer_Retour.Enabled = True
    End Sub
    Private Sub BtnRetour_MouseHover(sender As Object, e As EventArgs) Handles BtnRetour.MouseHover
        BtnRetour.BackgroundImage = My.Resources.b_btn_retour_sourisAuDessu
    End Sub
    Private Sub BtnRetour_MouseLeave(sender As Object, e As EventArgs) Handles BtnRetour.MouseLeave
        BtnRetour.BackgroundImage = My.Resources.a_btn_retour_nonEnfoncer
    End Sub

    'Timer btn
    Private Sub timer_Retour_Tick(sender As Object, e As EventArgs) Handles timer_Retour.Tick
        BtnRetour.BackgroundImage = My.Resources.b_btn_retour_sourisAuDessu
        timer_Retour.Enabled = False

        GlobalVariables.song.ClickSong(1, False)

        'on retourne a l'accueil mais avant on remet le pannel a "debutpartie" comme ca si le joueur reviens sur regle plus tard ca sera revenue au debut
        AffichageRegles.BackgroundImage = My.Resources.Deb_Partie_EXPLICATION
        Dim FormRegles As New MenuduJeu
        GlobalVariables.ShowForm(GlobalVariables.plein_ecran, FormRegles, Me)
        Me.Hide()
    End Sub

    'Gestion affichage des regles !

    'Regles de deb partie
    Private Sub BtnDebPartie_Click(sender As Object, e As EventArgs) Handles BtnDebPartie.Click
        AffichageRegles.BackgroundImage = My.Resources.Deb_Partie_EXPLICATION
    End Sub
    Private Sub BtnDebPartie_MouseHover(sender As Object, e As EventArgs) Handles BtnDebPartie.MouseHover
        BtnDebPartie.BackgroundImage = My.Resources.Deb_Partie_opa75
    End Sub
    Private Sub BtnDebPartie_MouseLeave(sender As Object, e As EventArgs) Handles BtnDebPartie.MouseLeave
        BtnDebPartie.BackgroundImage = My.Resources.Deb_Partie
    End Sub

    'Regles pendant un tour
    Private Sub BtnPendantTour_Click(sender As Object, e As EventArgs) Handles BtnPendantTour.Click
        AffichageRegles.BackgroundImage = My.Resources.pendantTour_EXPLICATION
    End Sub
    Private Sub BtnPendantTour_MouseHover(sender As Object, e As EventArgs) Handles BtnPendantTour.MouseHover
        BtnPendantTour.BackgroundImage = My.Resources.pendantTour_opa75
    End Sub
    Private Sub BtnPendantTour_MouseLeave(sender As Object, e As EventArgs) Handles BtnPendantTour.MouseLeave
        BtnPendantTour.BackgroundImage = My.Resources.pendantTour
    End Sub

    'regles calcul score
    Private Sub BtnCalcScore_Click(sender As Object, e As EventArgs) Handles BtnCalcScore.Click
        AffichageRegles.BackgroundImage = My.Resources.calcScore_EXPLICATION
    End Sub
    Private Sub BtnCalcScore_MouseHover(sender As Object, e As EventArgs) Handles BtnCalcScore.MouseHover
        BtnCalcScore.BackgroundImage = My.Resources.calcScore_opa75
    End Sub
    Private Sub BtnCalcScore_MouseLeave(sender As Object, e As EventArgs) Handles BtnCalcScore.MouseLeave
        BtnCalcScore.BackgroundImage = My.Resources.calcScore
    End Sub

    'regle fin partie
    Private Sub BtnFinPartie_Click(sender As Object, e As EventArgs) Handles BtnFinPartie.Click
        AffichageRegles.BackgroundImage = My.Resources.finpartie_EXPLICATION
    End Sub
    Private Sub BtnFinPartie_MouseHover(sender As Object, e As EventArgs) Handles BtnFinPartie.MouseHover
        BtnFinPartie.BackgroundImage = My.Resources.finpartie_opa75
    End Sub
    Private Sub BtnFinPartie_MouseLeave(sender As Object, e As EventArgs) Handles BtnFinPartie.MouseLeave
        BtnFinPartie.BackgroundImage = My.Resources.finpartie
    End Sub

    Private Sub Regles_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Lanceur.Close()
    End Sub

    Private Sub Regles_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class